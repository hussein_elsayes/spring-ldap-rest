package edu.ub.services.common;

public enum RequestType {
	CREATE_USER(1), CHANGE_ATTRIBUTES(2), CHANGE_PASSWORD(3), RESET_PASSWORD(4);

	private final int value;

	RequestType(int value)
	{
		this.value = value;
	}

	public int getValue()
	{
		return value;
	}
}
