package edu.ub.services.common;

import org.springframework.beans.factory.annotation.Autowired;

import com.mysql.jdbc.StringUtils;

import edu.ub.services.ldap.dto.Department;
import edu.ub.services.ldap.dto.LdapPerson;
import edu.ub.services.ldap.dto.RequestDetails;
import edu.ub.services.ldap.repositories.DepartmentRepository;

public class UserDetailsFactory {
	@Autowired
	DepartmentRepository depRepository;
	
	public RequestDetails create(LdapPerson person)
	{
		Department department = new Department();
		RequestDetails details = new RequestDetails();
		details.setSamAccount(person.getSamAccount());
		details.setFirstName(person.getFirstName());
		details.setLastName(person.getLastName());
		details.setArabicName(person.getArabicName());
		details.setMobile(person.getMobile());
		details.setIpPhone(person.getIpPhone());
		//set department as object
		//if values comes from AD then department will be null
		if(!StringUtils.isNullOrEmpty(person.getDepartment()))
		{
			System.err.println(person.getDepartment());
			department = depRepository.getDepartmentIdByLiteralValue("Rec_Off");
			System.err.println(department);
		}
		//if values comes from DB
		if(person.getDepId() != 0)
		{
			department = depRepository.findById(person.getDepId()).get();	
		}
		details.setDepartment(department);	
		details.setEmployeeID(person.getEmployeeID());
		details.setNationalId(person.getNationalId());
		details.setGender(person.getGender());		
		return details;
	}
}
