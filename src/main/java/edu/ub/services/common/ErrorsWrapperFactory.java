package edu.ub.services.common;

import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;

import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import edu.ub.services.ldap.dto.RequestDetails;
import edu.ub.services.ldap.dto.errors.FieldValidationError;
import edu.ub.services.ldap.dto.errors.ValidationErrorWrapper;

public class ErrorsWrapperFactory {

	public static ValidationErrorWrapper create(Set<ConstraintViolation<RequestDetails>> result) {
		ValidationErrorWrapper errorsWrapper = new ValidationErrorWrapper();
		for (ConstraintViolation<RequestDetails> cv : result) {
			errorsWrapper
					.addValidationError(new FieldValidationError(cv.getPropertyPath().toString(), cv.getMessage()));
		}
		return errorsWrapper;
	}

	public static ValidationErrorWrapper create(BindingResult bindingResult) {
		List<FieldError> fieldErrors = bindingResult.getFieldErrors();
		ValidationErrorWrapper errorsWrapper = new ValidationErrorWrapper();
		for (FieldError err : fieldErrors) {
			errorsWrapper.addValidationError(new FieldValidationError(err.getField(), err.getDefaultMessage()));
		}
		return errorsWrapper;
	}
}
