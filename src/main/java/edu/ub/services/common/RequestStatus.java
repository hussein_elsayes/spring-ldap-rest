package edu.ub.services.common;

public enum RequestStatus {
WAITING_VALIDATION(1),
VALIDATED(2),
DENIED(3),
CONFIRMED(4);

private int value;

RequestStatus(int value)
{
	this.value = value;
}

public int getValue()
{
	return value;
}


}
