package edu.ub.services.ldap.exceptions;

import org.springframework.security.core.AuthenticationException;

public class TokenException extends RuntimeException {

	public TokenException(String msg) {
		super(msg);
	}

}
