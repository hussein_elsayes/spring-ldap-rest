package edu.ub.services.ldap.exceptions;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;


@ResponseStatus(HttpStatus.NOT_FOUND)
public class BadOrFileNotFoundException extends RuntimeException {
    public BadOrFileNotFoundException(String message) {
        super(message);
    }

    public BadOrFileNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
