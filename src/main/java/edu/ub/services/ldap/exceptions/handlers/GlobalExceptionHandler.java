package edu.ub.services.ldap.exceptions.handlers;


import java.io.IOException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import edu.ub.services.ldap.dto.errors.ValidationErrorWrapper;
import edu.ub.services.ldap.exceptions.RequestDetailsValidationException;
import edu.ub.services.ldap.exceptions.UserExistsException;
import edu.ub.services.ldap.response.ApiResponse;
import edu.ub.services.ldap.response.ValidationErrorResponse;

@ControllerAdvice
public class GlobalExceptionHandler {
	
	@Autowired
	MessageSource messageSource;
	
	@ExceptionHandler
	public ResponseEntity<ValidationErrorResponse> handleError(RequestDetailsValidationException ex) {
		ValidationErrorWrapper errorsWrapper = ex.getErrorsWrapper();		
		ValidationErrorResponse errResp = new ValidationErrorResponse();
		errResp.setErrorsWrapper(errorsWrapper);
		errResp.setStatus(HttpStatus.BAD_REQUEST.value());
		errResp.setTimestamp(System.currentTimeMillis());
		System.err.println("Global Validation Happened !!");
		return new ResponseEntity<>(errResp, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler
	public ResponseEntity<ApiResponse> handleError(UserExistsException ex, Locale locale) {
		ApiResponse errResp = new ApiResponse(false, messageSource.getMessage("users.user.exists", null, locale));
		return new ResponseEntity<>(errResp, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler
	public ResponseEntity<ApiResponse> handleError(IOException ex, Locale locale) {
		ApiResponse errResp = new ApiResponse(false, messageSource.getMessage("requests.fileupload.error", null, locale));
		return new ResponseEntity<>(errResp, HttpStatus.BAD_REQUEST);
	}
	
/*	@ExceptionHandler
	public ResponseEntity<ApiResponse> handleError(Exception ex, Locale locale) {
		ApiResponse errResp = new ApiResponse(false, messageSource.getMessage("generic.error", null, locale));
		return new ResponseEntity<>(errResp, HttpStatus.BAD_REQUEST);
	}*/
	
	/*
	@ExceptionHandler
	public ResponseEntity<ApiResponse> handleError(InvalidCredentials ex) {
		ApiResponse errResp = new ApiResponse(false, "HUSSEIN CUSTOM INVALID CREDENTIALS MESSAGE !!");
		return new ResponseEntity<>(errResp, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler
	public ResponseEntity<ApiResponse> handleError(FileSizeLimitExceededException ex) {
		ApiResponse errResp = new ApiResponse(false, "File Exceeds !!");
		return new ResponseEntity<>(errResp, HttpStatus.BAD_REQUEST);
	}*/

}
