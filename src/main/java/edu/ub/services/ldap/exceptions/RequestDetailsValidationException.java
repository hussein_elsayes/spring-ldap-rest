package edu.ub.services.ldap.exceptions;

import edu.ub.services.ldap.dto.errors.ValidationErrorWrapper;

public class RequestDetailsValidationException extends RuntimeException {

	private ValidationErrorWrapper errorsWrapper;
	
	
	public RequestDetailsValidationException(String message,ValidationErrorWrapper errorsWrapper) {		
		super(message);
		this.errorsWrapper = errorsWrapper;
	}
	
	public RequestDetailsValidationException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public RequestDetailsValidationException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public RequestDetailsValidationException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public ValidationErrorWrapper getErrorsWrapper() {
		return errorsWrapper;
	}

	public void setErrorsWrapper(ValidationErrorWrapper errorsWrapper) {
		this.errorsWrapper = errorsWrapper;
	}


}
