package edu.ub.services.ldap.filters;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

@Component
public class CustomEntryPoint implements AuthenticationEntryPoint {

	@Autowired
	MessageSource messageSource;
	
	
    private static final Logger logger = LoggerFactory.getLogger(JwtAuthenticationEntryPoint.class);
	
	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authException) throws IOException, ServletException {
		logger.error("Responding with unauthorized error. Message - {}", authException.getMessage());
		System.err.println(authException.getClass());
		
		if(authException instanceof BadCredentialsException)
		{
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, messageSource.getMessage("login.invalid.credentials", null, new Locale("ar")));
		}else
		{
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, authException.getMessage());
		}
		
		
	}

}
