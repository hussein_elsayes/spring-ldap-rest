package edu.ub.services.ldap.filters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.ub.services.ldap.exceptions.TokenException;
import edu.ub.services.ldap.response.ApiResponse;
import edu.ub.services.ldap.service.impl.CustomUserDetailsService;
import edu.ub.services.ldap.service.impl.JwtTokenServiceImpl;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Order(1)
public class CustomJwtAuthenticationFilter extends OncePerRequestFilter {

	@Autowired
	private JwtTokenServiceImpl tokenProvider;

	@Autowired
	private CustomUserDetailsService customUserDetailsService;

	private static final Logger logger = LoggerFactory.getLogger(CustomJwtAuthenticationFilter.class);

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		try {
			String jwt = getJwtFromRequest(request);

			if (StringUtils.hasText(jwt) && tokenProvider.validateToken(jwt)) {
				String samAccount = tokenProvider.getUserIdFromJWT(jwt);

				UserDetails userDetails = customUserDetailsService.loadUserByUsername(samAccount);
				UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
						userDetails, null, userDetails.getAuthorities());
				authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

				SecurityContextHolder.getContext().setAuthentication(authentication);
				filterChain.doFilter(request, response);
			}
		} catch (ExpiredJwtException ex) {
			System.err.println("HUSSEIN : EXPIRED TOKEN EXCETPTION");
			ApiResponse errResponse = new ApiResponse(false, "HUSSEIN : EXPIRED TOKEN EXCETPTION");
			response.setStatus(HttpStatus.BAD_REQUEST.value());
			response.getWriter().write(convertObjectToJson(errResponse));
		} catch (SignatureException ex) {
			System.err.println("HUSSEIN : Invalid JWT signature");
			ApiResponse errResponse = new ApiResponse(false, "HUSSEIN : Invalid JWT signature");
			response.setStatus(HttpStatus.BAD_REQUEST.value());
			response.getWriter().write(convertObjectToJson(errResponse));
		} catch (MalformedJwtException ex) {
			System.err.println("HUSSEIN : Invalid JWT token");
			ApiResponse errResponse = new ApiResponse(false, "HUSSEIN : Invalid JWT token");
			response.setStatus(HttpStatus.BAD_REQUEST.value());
			response.getWriter().write(convertObjectToJson(errResponse));
		} catch (UnsupportedJwtException ex) {
			System.err.println("HUSSEIN : Unsupported JWT token");
			ApiResponse errResponse = new ApiResponse(false, "HUSSEIN : Unsupported JWT token");
			response.setStatus(HttpStatus.BAD_REQUEST.value());
			response.getWriter().write(convertObjectToJson(errResponse));
		} catch (IllegalArgumentException ex) {
			System.err.println("HUSSEIN : JWT claims string is empty.");
			ApiResponse errResponse = new ApiResponse(false, "HUSSEIN : JWT claims string is empty");
			response.setStatus(HttpStatus.BAD_REQUEST.value());
			response.getWriter().write(convertObjectToJson(errResponse));
		} catch (Exception ex) {
			// logger.error("Could not set user authentication in security context", ex);
		}
	}

	private String getJwtFromRequest(HttpServletRequest request) {
		String bearerToken = request.getHeader("Authorization");
		if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
			return bearerToken.substring(7, bearerToken.length());
		}
		return null;
	}

	public String convertObjectToJson(Object object) throws JsonProcessingException {
		if (object == null) {
			return null;
		}
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(object);
	}

}
