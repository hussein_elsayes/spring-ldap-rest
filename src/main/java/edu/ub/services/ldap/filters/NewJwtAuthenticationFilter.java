package edu.ub.services.ldap.filters;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.ub.services.ldap.exceptions.CustomAuthenticationException;
import edu.ub.services.ldap.response.ApiResponse;
import edu.ub.services.ldap.service.JwtTokenService;
import edu.ub.services.ldap.service.impl.CustomUserDetailsService;
import edu.ub.services.ldap.service.impl.JwtTokenServiceImpl;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

public class NewJwtAuthenticationFilter extends OncePerRequestFilter {
	
	@Autowired
	JwtTokenService jwtTokenService;
	@Autowired
	CustomUserDetailsService customUserDetailsService;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		System.out.println("Inside Authorization Filter !!");

		try {
			String jwt = getJwtFromRequest(request);
			System.out.println("JWT CAPTURED FROM THE FILTER IS :    " + jwt);

			System.out.println("IF JWT STRING IS NOT EMPTY ?  " + StringUtils.hasText(jwt));
			if (jwt != null &&  StringUtils.hasText(jwt) && jwtTokenService.validateToken(jwt)) {
				System.out.println("Valid JWT !!");
				String samAccount = jwtTokenService.getUserIdFromJWT(jwt);
				UserDetails userDetails = customUserDetailsService.loadUserByUsername(samAccount);
				UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
				//authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
				for(GrantedAuthority authority : authentication.getAuthorities())
				{
					System.err.println("Authority From Security Context" + authority.getAuthority());
				}
				SecurityContextHolder.getContext().setAuthentication(authentication);
			} else {
				System.err.println("AUTHENTICATION HEADER IS MISSING !");
			}

			
		} catch (ExpiredJwtException ex) {
			System.err.println("HUSSEIN : EXPIRED TOKEN EXCETPTION");
			//throw new CustomAuthenticationException("EXPIRED TOKEN EXCETPTION");
			//ApiResponse errResponse = new ApiResponse(false, "HUSSEIN : EXPIRED TOKEN EXCETPTION");
			//response.setStatus(HttpStatus.BAD_REQUEST.value());
			//response.getWriter().write(convertObjectToJson(errResponse));
		} catch (SignatureException ex) {
			System.err.println("HUSSEIN : Invalid JWT signature");
			//throw new CustomAuthenticationException("Invalid JWT signature");
			//ApiResponse errResponse = new ApiResponse(false, "HUSSEIN : Invalid JWT signature");
			//response.setStatus(HttpStatus.BAD_REQUEST.value());
			//response.getWriter().write(convertObjectToJson(errResponse));
		} catch (MalformedJwtException ex) {
			System.err.println("HUSSEIN : Invalid JWT token");
			//throw new CustomAuthenticationException("Invalid JWT token");
			//ApiResponse errResponse = new ApiResponse(false, "HUSSEIN : Invalid JWT token");
			//response.setStatus(HttpStatus.BAD_REQUEST.value());
			//response.getWriter().write(convertObjectToJson(errResponse));
		} catch (UnsupportedJwtException ex) {
			System.err.println("HUSSEIN : Unsupported JWT token");
			//throw new CustomAuthenticationException("Unsupported JWT token");
			//ApiResponse errResponse = new ApiResponse(false, "HUSSEIN : Unsupported JWT token");
			//response.setStatus(HttpStatus.BAD_REQUEST.value());
			//response.getWriter().write(convertObjectToJson(errResponse));
		} catch (IllegalArgumentException ex) {
			System.err.println("HUSSEIN : JWT claims string is empty.");
			//throw new CustomAuthenticationException("JWT claims string is empty.");
			//ApiResponse errResponse = new ApiResponse(false, "HUSSEIN : JWT claims string is empty");
			//response.setStatus(HttpStatus.BAD_REQUEST.value());
			//response.getWriter().write(convertObjectToJson(errResponse));
		} catch (Exception ex) {
			logger.error("Could not set user authentication in security context", ex);
			//throw new CustomAuthenticationException("Authentication Error");
		}
		
		System.err.println("BEFORE FILTER CHAIN CALL !");
		filterChain.doFilter(request, response);
	}

	private String getJwtFromRequest(HttpServletRequest request) {
		String bearerToken = request.getHeader("Authorization");
		if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
			return bearerToken.substring(7, bearerToken.length());
		}
		return null;
	}

	public String convertObjectToJson(Object object) throws JsonProcessingException {
		if (object == null) {
			return null;
		}
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(object);
	}

}
