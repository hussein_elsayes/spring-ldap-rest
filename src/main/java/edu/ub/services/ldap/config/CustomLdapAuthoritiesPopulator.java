package edu.ub.services.ldap.config;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.ldap.userdetails.LdapAuthoritiesPopulator;

import edu.ub.services.ldap.service.impl.CustomUserDetailsService;

public class CustomLdapAuthoritiesPopulator implements LdapAuthoritiesPopulator {

	@Autowired
	CustomUserDetailsService customUserDetailsService;

	@Override
	public Collection<? extends GrantedAuthority> getGrantedAuthorities(DirContextOperations userData,
			String username) {
		UserDetails details = customUserDetailsService.loadUserByUsername(username);
		return details.getAuthorities();
	}

}
