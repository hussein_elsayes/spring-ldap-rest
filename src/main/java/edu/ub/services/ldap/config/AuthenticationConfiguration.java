package edu.ub.services.ldap.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.authentication.configurers.ldap.LdapAuthenticationProviderConfigurer;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.ldap.DefaultSpringSecurityContextSource;

@Configuration
@EnableWebSecurity
public class AuthenticationConfiguration extends GlobalAuthenticationConfigurerAdapter {
	@Autowired
	private Environment env;

	@Bean
	public CustomLdapAuthoritiesPopulator authoritiesPopulator()
	{
		return new CustomLdapAuthoritiesPopulator();
	}
	
	@Bean
	public LdapContextSource contextSource() {
		LdapContextSource contextSource = new LdapContextSource();
		contextSource.setUrl(env.getRequiredProperty("ldap.url"));
		contextSource.setBase(env.getRequiredProperty("ldap.partitionSuffix"));
		contextSource.setUserDn(env.getRequiredProperty("ldap.principal"));
		contextSource.setPassword(env.getRequiredProperty("ldap.password"));
		contextSource.setReferral("follow");
		contextSource.afterPropertiesSet();
		return contextSource;
	}

	@Bean
	public LdapTemplate ldapTemplate() {
		return new LdapTemplate(contextSource());
	}

	@Override
	public void init(AuthenticationManagerBuilder auth) throws Exception {

		DefaultSpringSecurityContextSource contextSource = new DefaultSpringSecurityContextSource(
				env.getRequiredProperty("ldap.url"));
		contextSource.setBase(env.getRequiredProperty("ldap.partitionSuffix"));
		contextSource.setUserDn(env.getRequiredProperty("ldap.principal"));
		contextSource.setPassword(env.getRequiredProperty("ldap.password"));
		contextSource.setReferral("follow");
		contextSource.afterPropertiesSet();

		LdapAuthenticationProviderConfigurer<AuthenticationManagerBuilder> ldapAuthenticationProviderConfigurer = auth
				.ldapAuthentication().ldapAuthoritiesPopulator(authoritiesPopulator()).rolePrefix("ROLE_");

		ldapAuthenticationProviderConfigurer.userSearchFilter(env.getRequiredProperty("ldap.user_search_filter"))
				.userSearchBase("").contextSource(contextSource);
	}

}
