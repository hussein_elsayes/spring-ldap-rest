package edu.ub.services.ldap.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import edu.ub.services.ldap.filters.CustomEntryPoint;
import edu.ub.services.ldap.filters.NewJwtAuthenticationFilter;
import edu.ub.services.ldap.service.JwtTokenService;
import edu.ub.services.ldap.service.LdapPersonService;
import edu.ub.services.ldap.service.impl.CustomUserDetailsService;
import edu.ub.services.ldap.service.impl.JwtTokenServiceImpl;
import edu.ub.services.ldap.service.impl.LdapPersonServiceImpl;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)

public class LdapConfig extends WebSecurityConfigurerAdapter {


	@Bean
	public LdapPersonService personService()
	{
		return new LdapPersonServiceImpl();
	}
	
	

	@Bean
	public CustomUserDetailsService customUserDetailsService()
	{
		return new CustomUserDetailsService();
	}
	
	@Bean
	public CustomLdapAuthoritiesPopulator authoritiesPopulator()
	{
		return new CustomLdapAuthoritiesPopulator();
	}

	@Bean
	public JwtTokenService tokenProvider()
	{
		return new JwtTokenServiceImpl();
	}
	
	@Bean
	NewJwtAuthenticationFilter authFilter()
	{
		return new NewJwtAuthenticationFilter();
	}
	
	@Bean
	AuthenticationEntryPoint authenticationEntryPoint() {
		return new CustomEntryPoint();
	}

	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception
	{
		return super.authenticationManagerBean();
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.cors().and().csrf().disable()
		.exceptionHandling().authenticationEntryPoint(authenticationEntryPoint())
		.and()
		.authorizeRequests().antMatchers("/api/**/auth/**").fullyAuthenticated()
        .anyRequest().permitAll()
        .and()
        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		// Add our custom JWT security filter
        http.addFilterBefore(authFilter(), UsernamePasswordAuthenticationFilter.class);
	}
	



}
