package edu.ub.services.ldap.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.ldap.repository.config.EnableLdapRepositories;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import edu.ub.services.common.UserDetailsFactory;
import edu.ub.services.ldap.service.FileStorageService;
import edu.ub.services.ldap.service.LdapPersonService;
import edu.ub.services.ldap.service.SmsService;
import edu.ub.services.ldap.service.UserService;
import edu.ub.services.ldap.service.impl.FileStorageServiceImpl;
import edu.ub.services.ldap.service.impl.LdapPersonServiceImpl;
import edu.ub.services.ldap.service.impl.Mobily;
import edu.ub.services.ldap.service.impl.SmsServiceImpl;
import edu.ub.services.ldap.service.impl.UserServiceImpl;

@Configuration
@EntityScan("edu.ub.services.ldap.entity")
@ComponentScan(basePackages= "edu.ub.services")
@EnableJpaAuditing
@EnableGlobalMethodSecurity( securedEnabled = true, jsr250Enabled = true, prePostEnabled = true)
@EnableLdapRepositories(basePackages = "edu.ub.services.ldap.reositories")

public class AppConfig {
	
	// for javax.validation Constraints to read from by @Valid annotation
	@Bean
	public MessageSource messageSource() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasename("classpath:messages");
		messageSource.setDefaultEncoding("UTF-8");
		return messageSource;
	}

	@Bean
	public LocalValidatorFactoryBean validator() {
		LocalValidatorFactoryBean bean = new LocalValidatorFactoryBean();
		bean.setValidationMessageSource(messageSource());
		return bean;
	}

	// for person service
	@Bean
	public LdapPersonService personService() {
		return new LdapPersonServiceImpl();
	}

	// for userRequest service
	@Bean
	public UserService userRequestService() {
		return new UserServiceImpl();
	}
	
	
	@Bean
	public SmsService smsService()
	{
		return new SmsServiceImpl();
	}
	
	@Bean
	public UserDetailsFactory userDetailsFactory()
	{
		return new UserDetailsFactory();
	}
	
	@Bean
	public FileStorageService fileStorageService()
	{
		return new FileStorageServiceImpl();
	}
	
	@Bean
	public Mobily mobily()
	{
		return new Mobily();
	}

	/*@Bean
	public CommonsMultipartResolver multipartResolver() {
	    CommonsMultipartResolver resolver=new CommonsMultipartResolver();
	    resolver.setDefaultEncoding("utf-8");
	    return resolver;
	}*/
	

}
