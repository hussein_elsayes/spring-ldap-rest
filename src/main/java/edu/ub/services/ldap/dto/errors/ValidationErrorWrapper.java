package edu.ub.services.ldap.dto.errors;

import java.util.ArrayList;
import java.util.List;

public class ValidationErrorWrapper {

	private List<FieldValidationError> errors;

	public void addValidationError(FieldValidationError error) {
		if(this.errors == null)
		{
			errors = new ArrayList<>();
		}
		errors.add(error);
	}

	public List<FieldValidationError> getErrors() {
		return errors;
	}

	public void setErrors(List<FieldValidationError> errors) {
		this.errors = errors;
	}	
}
