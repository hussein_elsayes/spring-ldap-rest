package edu.ub.services.ldap.dto;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="DEPARTMENT")
public class Department {
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "DEP_ID")
	private long id;
	@Column(name = "DEP_NAME")
	private String depName;
	@Column(name = "DEP_NAME_AR")
	private String depName_ar;
	@Column(name = "DEP_NAME_AD")
	private String depName_AD;
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="OU")
	private OrganizationalUnit ou;
	@JsonIgnore
	@OneToMany(mappedBy = "department",cascade=CascadeType.ALL)
	private Set<RequestDetails> userDetails;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getDepName() {
		return depName;
	}
	public void setDepName(String depName) {
		this.depName = depName;
	}	
	public String getDepName_ar() {
		return depName_ar;
	}
	public void setDepName_ar(String depName_ar) {
		this.depName_ar = depName_ar;
	}
	public String getDepName_AD() {
		return depName_AD;
	}
	public void setDepName_AD(String depName_AD) {
		this.depName_AD = depName_AD;
	}
	public OrganizationalUnit getOu() {
		return ou;
	}
	public void setOu(OrganizationalUnit ou) {
		this.ou = ou;
	}
	public Set<RequestDetails> getUserDetails() {
		return userDetails;
	}
	public void setUserDetails(Set<RequestDetails> userDetails) {
		this.userDetails = userDetails;
	}
	
	
	
}
