package edu.ub.services.ldap.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "created", "lastModified" }, allowGetters = true)
@Table(name = "REQUEST")
public class UserRequest {

	public UserRequest() {

	}

	public UserRequest(int reqType, int reqStatus, RequestDetails reqDetails) {
		super();
		this.reqType = reqType;
		this.reqStatus = reqStatus;
		this.reqDetails = reqDetails;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "REQ_ID")
	private long id;

	@Column(name = "REQUEST_TYPE")
	private int reqType;

	@Column(name = "REQUEST_STATUS")
	private int reqStatus;

	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATED", nullable = false, updatable = false)
	private Date created;

	@LastModifiedDate
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "MODIFIED", nullable = false)
	private Date lastModified;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "REQUEST_DETAILS", referencedColumnName = "id")
	private RequestDetails reqDetails;

	@OneToMany(mappedBy = "userRequest", cascade = { CascadeType.MERGE, CascadeType.DETACH, CascadeType.PERSIST,
			CascadeType.REFRESH })
	private List<Vcode> verificationCodes = new ArrayList<>();

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "attachment")
	private Attachment attachment;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getReqType() {
		return reqType;
	}

	public void setReqType(int reqType) {
		this.reqType = reqType;
	}

	public int getReqStatus() {
		return reqStatus;
	}

	public void setReqStatus(int reqStatus) {
		this.reqStatus = reqStatus;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getLastModified() {
		return lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public RequestDetails getReqDetails() {
		return reqDetails;
	}

	public void setReqDetails(RequestDetails reqDetails) {
		this.reqDetails = reqDetails;
	}

	public Attachment getAttachment() {
		return attachment;
	}

	public void setAttachment(Attachment attachment) {
		this.attachment = attachment;
	}

	public void add(Vcode vcode) {
		if (verificationCodes == null) {
			verificationCodes = new ArrayList<>();
		}
		// here setting vcode into the userrequest object
		verificationCodes.add(vcode);
		// setting the user request also in the vcode object
		vcode.setUserRequest(this);
	}

}
