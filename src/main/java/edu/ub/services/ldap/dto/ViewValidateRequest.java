package edu.ub.services.ldap.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class ViewValidateRequest {

	//@NotNull(message="{request.validate.id.blank}")
	private long id;
	//@NotBlank(message="{request.validate.code.blank}")
	private String vcode;
	public ViewValidateRequest(long id, String vcode) {

		this.id = id;
		this.vcode = vcode;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	
	
}
