package edu.ub.services.ldap.dto;

import javax.naming.Name;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;

import org.springframework.ldap.odm.annotations.Attribute;
import org.springframework.ldap.odm.annotations.Entry;
import org.springframework.ldap.odm.annotations.Id;
import org.springframework.ldap.odm.annotations.Transient;
import org.springframework.ldap.support.LdapNameBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entry(objectClasses = { "person", "top" })
public class LdapPerson {

	@Id
	@JsonIgnore
	private Name dn;
	@Attribute(name = "sAMAccountName")
	private String samAccount;
	@Attribute(name = "givenName")
	private String firstName;
	@Attribute(name = "sn")
	private String lastName;
	@Attribute(name = "displayName")
	private String englishName;
	@Attribute(name = "description")
	private String arabicName;
	@Attribute(name = "mail")
	private String email;
	@Attribute(name = "employeeID")
	private String employeeID;
	@Attribute(name = "internationalISDNNumber")
	private String nationalId;
	@Attribute(name = "department")
	private String department;
	@Attribute(name = "mobile")
	private String mobile;
	@Attribute(name = "ipPhone")
	private String ipPhone;
	@Attribute(name = "info")
	private String gender;

	@Transient
	private long depId;
	@Transient
	private long ouId;
	@JsonIgnore
	@Transient
	private String[] membership;

	public static LdapPerson create(UserRequest request) {
		// set ldapPerson DN
		String organizationalUnit = request.getReqDetails().getDepartment().getOu().getOuName_AD();
		String department = request.getReqDetails().getDepartment().getDepName_AD();
		// get the details
		RequestDetails details = request.getReqDetails();
		Name userDN = LdapNameBuilder.newInstance().add("OU", organizationalUnit).add("OU", department).add("CN", details.getSamAccount()).build();
		// bake the person object
		LdapPerson ldapPerson = new LdapPerson();
		ldapPerson.setDn(userDN);
		ldapPerson.setSamAccount(details.getSamAccount());
		ldapPerson.setFirstName(details.getFirstName());
		ldapPerson.setLastName(details.getLastName());
		ldapPerson.setEnglishName(details.getFirstName() + " " + details.getLastName());
		ldapPerson.setArabicName(details.getArabicName());
		ldapPerson.setEmail(details.getSamAccount() + "@ub.edu.sa");
		ldapPerson.setEmployeeID(details.getEmployeeID());
		ldapPerson.setNationalId(details.getNationalId());
		ldapPerson.setDepartment(details.getDepartment().getDepName_AD());
		ldapPerson.setMobile(details.getMobile());
		ldapPerson.setIpPhone(details.getIpPhone());
		ldapPerson.setGender(details.getGender());
		return ldapPerson;
	}
	
	public static Attributes getAttributes(LdapPerson person)
	{
		Attributes attrs = new BasicAttributes();
		BasicAttribute ocattr = new BasicAttribute("objectclass");
		ocattr.add("top");
		ocattr.add("person");
		ocattr.add("organizationalperson");
		ocattr.add("user");
		attrs.put(ocattr);
		
		attrs.put("cn", person.getSamAccount());
		attrs.put("sAMAccountName", person.getSamAccount());
		attrs.put("givenName", person.getFirstName());
		attrs.put("sn", person.getLastName());
		attrs.put("displayName", person.getEnglishName());
		attrs.put("description", person.getArabicName());
		attrs.put("mail", person.getEmail());
		attrs.put("employeeID", person.getEmployeeID());
		attrs.put("internationalISDNNumber", person.getNationalId());
		attrs.put("department", person.getDepartment());
		attrs.put("mobile", person.getMobile());
		attrs.put("ipPhone", person.getIpPhone());
		attrs.put("info", person.getGender());		
		//attrs.put("userAccountControl", "0x0200"); //setting user to normal account
		
		return attrs;
	}

	public LdapPerson() {

	}

	public Name getDn() {
		return dn;
	}

	public void setDn(Name dn) {
		this.dn = dn;
	}

	public String getSamAccount() {
		return samAccount;
	}

	public void setSamAccount(String samAccount) {
		this.samAccount = samAccount;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEnglishName() {
		return englishName;
	}

	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}

	public String getArabicName() {
		return arabicName;
	}

	public void setArabicName(String arabicName) {
		this.arabicName = arabicName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(String employeeID) {
		this.employeeID = employeeID;
	}

	public String getNationalId() {
		return nationalId;
	}

	public void setNationalId(String nationalId) {
		this.nationalId = nationalId;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getIpPhone() {
		return ipPhone;
	}

	public void setIpPhone(String ipPhone) {
		this.ipPhone = ipPhone;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
	

	public long getDepId() {
		return depId;
	}

	public void setDepId(long depId) {
		this.depId = depId;
	}
	

	public long getOuId() {
		return ouId;
	}

	public void setOuId(long ouId) {
		this.ouId = ouId;
	}

	public String[] getMembership() {
		return membership;
	}

	public void setMembership(String[] membership) {
		this.membership = membership;
	}

	@Override
	public String toString() {
		return "LdapPerson [dn=" + dn + ", samAccount=" + samAccount + ", firstName=" + firstName + ", lastName="
				+ lastName + ", englishName=" + englishName + ", arabicName=" + arabicName + ", email=" + email
				+ ", employeeID=" + employeeID + ", nationalId=" + nationalId + ", department=" + department
				+ ", mobile=" + mobile + ", ipPhone=" + ipPhone + ", gender=" + gender + "]";
	}

}
