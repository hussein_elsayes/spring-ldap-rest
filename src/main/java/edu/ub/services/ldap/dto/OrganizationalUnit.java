package edu.ub.services.ldap.dto;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="OU")
public class OrganizationalUnit {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "OU_ID")
	private long id;
	@Column(name = "OU_NAME")
	private String ouName;
	@Column(name = "OU_NAME_AR")
	private String ouNameAr;
	@Column(name = "OU_NAME_AD")
	private String ouName_AD;
	@OneToMany(mappedBy="ou",cascade = CascadeType.ALL)// mapped by needs object to map with not column
	private Set<Department> departments;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getOuName() {
		return ouName;
	}
	public void setOuName(String ouName) {
		this.ouName = ouName;
	}
	public String getOuNameAr() {
		return ouNameAr;
	}
	public void setOuNameAr(String ouNameAr) {
		this.ouNameAr = ouNameAr;
	}
	public String getOuName_AD() {
		return ouName_AD;
	}
	public void setOuName_AD(String ouName_AD) {
		this.ouName_AD = ouName_AD;
	}
	public Set<Department> getDepartments() {
		return departments;
	}
	public void setDepartments(Set<Department> departments) {
		this.departments = departments;
	}
	
	
}
