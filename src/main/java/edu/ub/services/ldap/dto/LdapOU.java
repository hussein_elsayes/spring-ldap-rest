package edu.ub.services.ldap.dto;

import javax.naming.Name;

import org.springframework.ldap.odm.annotations.Attribute;
import org.springframework.ldap.odm.annotations.DnAttribute;
import org.springframework.ldap.odm.annotations.Entry;
import org.springframework.ldap.odm.annotations.Id;

@Entry(objectClasses = {"organizationalUnit", "top"})
public final class LdapOU {
    @Id
    private Name dn;

    @Attribute(name = "description")
    private String description;

    @Attribute(name = "distinguishedName")
    private String distinguishedName;
    
    public String getDistinguishedName() {
		return distinguishedName;
	}

	public void setDistinguishedName(String distinguishedName) {
		this.distinguishedName = distinguishedName;
	}

	@Attribute(name = "ou")
    private String ou;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Name getDn() {
        return dn;
    }

    public void setDn(Name dn) {
        this.dn = dn;
    }

	public String getOu() {
		return ou;
	}

	public void setOu(String ou) {
		this.ou = ou;
	}

	@Override
	public String toString() {
		return "LdapOU [dn=" + dn + ", description=" + description + ", ou=" + ou + "]";
	}
	
	
}