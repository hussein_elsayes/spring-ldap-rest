package edu.ub.services.ldap.dto.errors;

public class FieldValidationError {

	private String fieldName;
	private String validationMessage;

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getValidationMessage() {
		return validationMessage;
	}

	public void setValidationMessage(String validationMessage) {
		this.validationMessage = validationMessage;
	}

	public FieldValidationError(String fieldName, String validationMessage) {
		this.fieldName = fieldName;
		this.validationMessage = validationMessage;
	}
}
