package edu.ub.services.ldap.dto;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonIgnore;

import edu.ub.services.ldap.repositories.DepartmentRepository;



@Entity
@Table(name = "REQUEST_DETAILS")
public class RequestDetails {
	
	public RequestDetails() {

	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonIgnore
	private long id;

	//@NotBlank(message = "{request.details.validation.sam.account.blank}")
	@Column(name = "SAM_ACCOUNT")
	private String samAccount;

	@NotBlank(message = "{request.details.validation.first.name.blank}")
	@Size(min = 3, message = "{request.details.validation.first.name.min}")
	@Size(max = 40 , message = "{request.details.validation.first.name.max}")
	@Column(name = "FIRST_NAME")
	private String firstName;

	@NotBlank(message = "{request.details.validation.last.name.blank}")
	@Size(min = 3, message = "{request.details.validation.last.name.min}")
	@Size(max = 40 , message = "{request.details.validation.last.name.max}")
	@Column(name = "LAST_NAME")
	private String lastName;


	@NotBlank(message = "{request.details.validation.arabic.name.blank}")
	@Size(min = 3, message = "{request.details.validation.arabic.name.min}")
	@Size(max = 40 , message = "{request.details.validation.arabic.name.max}")
	@Pattern(regexp = "^((:?\\s*[\\u0600-\\u06FF]\\s*)+)$", message = "{request.details.validation.arabic.name.pattern}")
	@Column(name = "ARABIC_NAME")
	private String arabicName;

	@NotBlank(message = "{request.details.validation.mobile.blank}")
	@Pattern(regexp = "^[5][0-9]{8}$", message = "{request.details.validation.mobile.pattern}")
	@Column(name = "MOBILE")
	private String mobile;

	//@NotBlank(message = "{request.details.validation.ip.phone.blank}")
	@Pattern(regexp = "^[0-9]{4}$", message = "{request.details.validation.ip.phone.pattern}")
	@Column(name = "IP_PHONE")
	private String ipPhone;
	
	//@NotNull(message = "{request.details.validation.department.blank}")
	@ManyToOne
	@JoinColumn(name = "DEPARTMENT")
	private Department department;
	
	@NotNull(message = "{request.details.validation.department.blank}")
	@Transient
	private int depId;

	@NotBlank(message = "{request.details.validation.employee.id.blank}")
	@Pattern(regexp = "^[0-9]{1,6}$", message = "{request.details.validation.employee.id.pattern}")
	@Column(name = "EMPLOYEE_ID")
	private String employeeID;

	@NotBlank(message = "{request.details.validation.national.id.blank}")
	@Pattern(regexp = "^[1-2][0-9]{9}$", message = "{request.details.validation.national.id.pattern}")
	@Column(name = "NATIONAL_ID")
	private String nationalId;

	@NotBlank(message = "{request.details.validation.gender.blank}")
	@Pattern(regexp = "[1-2]", message = "{request.details.validation.gender.pattern}")
	@Column(name = "GENDER")
	private String gender;

	@JsonIgnore
	@OneToOne(mappedBy = "reqDetails", cascade = CascadeType.ALL)
	private UserRequest userRequest;
	
	

	
	
	public UserRequest getUserRequest() {
		return userRequest;
	}

	public void setUserRequest(UserRequest userRequest) {
		this.userRequest = userRequest;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSamAccount() {
		return samAccount;
	}

	public void setSamAccount(String samAccount) {
		this.samAccount = samAccount;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String fistName) {
		this.firstName = fistName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getArabicName() {
		return arabicName;
	}

	public void setArabicName(String arabicName) {
		this.arabicName = arabicName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getIpPhone() {
		return ipPhone;
	}

	public void setIpPhone(String ipPhone) {
		this.ipPhone = ipPhone;
	}

	public Department getDepartment() {
		return department;
	}

	public int getDepId() {
		return depId;
	}

	public void setDepId(int depId) {
		this.depId = depId;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public String getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(String employeeID) {
		this.employeeID = employeeID;
	}

	public String getNationalId() {
		return nationalId;
	}

	public void setNationalId(String nationalId) {
		this.nationalId = nationalId;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
	
	

}
