package edu.ub.services.ldap.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.ub.services.ldap.dto.RequestDetails;

public interface UserDetailsRepository extends JpaRepository<RequestDetails,Long>{
	
}
