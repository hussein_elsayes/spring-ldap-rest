package edu.ub.services.ldap.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import edu.ub.services.ldap.dto.UserRequest;

@Repository
public interface RequestRepository extends JpaRepository<UserRequest, Long> {
	
	@Query(value = "SELECT r.req_id , r.created , r.modified , r.request_status , r.request_type, r.attachment, r.request_details FROM request_details d INNER JOIN request r ON d.id = r.request_details where r.request_type = \"1\"  AND r.request_status = \"2\" " , nativeQuery = true)
	public List<UserRequest> getWaitingConfirmationCreateUserRequests();
	
	@Query(value = "SELECT r.req_id , r.created , r.modified , r.request_status , r.request_type, r.attachment, r.request_details FROM request_details d INNER JOIN request r ON d.id = r.request_details where d.mobile =?1 AND d.national_id =?2 AND d.employee_id =?3 AND r.request_status = \"1\" AND r.created = (SELECT MAX(created) FROM request)" , nativeQuery = true)
	public List<UserRequest> getNonValidatedRequests(String mobileNo,String nationalId,String employeeId);
	
	@Query(value = "SELECT COUNT(*) FROM request_details d INNER JOIN request r ON d.id = r.request_details where d.sam_account =?1 AND r.request_status = \"2\" " , nativeQuery = true)
	public int getWaitingConfirmationRequest(String samAccount);
	
	@Query(value = "SELECT COUNT(*) FROM request_details d INNER JOIN request r ON d.id = r.request_details where date(r.created) = CURDATE() AND d.mobile =?1 " , nativeQuery = true)
	public int getRequestsPerDayByMobile(String mobileNo);
	
	@Query(value = "SELECT COUNT(*) FROM request_details d INNER JOIN request r ON d.id = r.request_details where date(r.created) = CURDATE() AND d.sam_account =?1 " , nativeQuery = true)
	public int getRequestsPerDayBySamAccount(String samAccount);
	
	@Query(value = "SELECT COUNT(*) FROM request_details d INNER JOIN request r ON d.id = r.request_details where d.mobile =?1 AND r.request_type = \"1\" AND r.request_status = \"2\" " , nativeQuery = true)
	public int getRequestsByMobileNumber(String mobileNo);
}
