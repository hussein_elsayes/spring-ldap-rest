package edu.ub.services.ldap.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.ub.services.ldap.dto.OrganizationalUnit;



public interface OURepository extends JpaRepository<OrganizationalUnit,Long> {

}
