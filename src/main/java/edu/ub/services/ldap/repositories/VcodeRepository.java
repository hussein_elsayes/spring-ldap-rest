package edu.ub.services.ldap.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.ub.services.ldap.dto.Vcode;

public interface VcodeRepository extends JpaRepository<Vcode, Long> {

}
