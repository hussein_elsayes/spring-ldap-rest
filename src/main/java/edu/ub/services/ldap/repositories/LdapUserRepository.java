package edu.ub.services.ldap.repositories;

import org.springframework.data.ldap.repository.LdapRepository;

import edu.ub.services.ldap.dto.LdapPerson;

public interface LdapUserRepository extends LdapRepository<LdapPerson> {

}
