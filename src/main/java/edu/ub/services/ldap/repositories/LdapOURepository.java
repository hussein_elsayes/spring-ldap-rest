package edu.ub.services.ldap.repositories;

import org.springframework.data.ldap.repository.LdapRepository;
import org.springframework.data.ldap.repository.Query;
import org.springframework.stereotype.Repository;

import edu.ub.services.ldap.dto.LdapOU;

@Repository
public interface LdapOURepository extends LdapRepository<LdapOU> {

	@Query("(ou={0})")
	LdapOU findByName(String ou);
	
}
