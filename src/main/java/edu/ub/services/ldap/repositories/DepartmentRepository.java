package edu.ub.services.ldap.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import edu.ub.services.ldap.dto.Department;

public interface DepartmentRepository extends JpaRepository<Department,Long> {

	//indexed parameter ( using parameters by index not by @Param('paramName')
	@Query(value = "SELECT * FROM user_service.department d WHERE d.ou = ?1 ", nativeQuery = true)
	public List<Department> getDepartmentsByOuId(long id);
	
	@Query(value = "SELECT * FROM user_service.department d WHERE d.dep_name_ad = ?1 ", nativeQuery = true)
	public Department getDepartmentIdByLiteralValue(String dep_name_ad);
}
