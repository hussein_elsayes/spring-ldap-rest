package edu.ub.services.ldap.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.ub.services.ldap.dto.Attachment;

public interface AttachmentRepository extends JpaRepository<Attachment, Long> {

}
