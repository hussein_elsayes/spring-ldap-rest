package edu.ub.services.ldap.service.impl;

import java.util.Date;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;

import edu.ub.services.ldap.service.JwtTokenService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

@PropertySource("classpath:ldap.properties")
public class JwtTokenServiceImpl implements JwtTokenService{
	private static final Logger logger = LoggerFactory.getLogger(JwtTokenServiceImpl.class);

	@Value("${app.jwtSecret}")
	private String jwtSecret;

	@Value("${app.jwtExpirationInSeconds}")
	private int jwtExpirationInSeconds;

	public String generateToken(String samAccount)  {
		System.out.println("First Line of JWT Token Provider ");
		// CustomPrincipal userPrincipal = (CustomPrincipal)
		// authentication.getPrincipal();
		String id = UUID.randomUUID().toString().replace("-", "");
		Date now = new Date();
		Date expiryDate = new Date(System.currentTimeMillis() + (1000 * jwtExpirationInSeconds));
		System.out.println("Before Return !");
		return Jwts.builder().setId(id).setIssuedAt(now).setNotBefore(now).setSubject(samAccount)
				.setExpiration(expiryDate).signWith(SignatureAlgorithm.HS512, jwtSecret).compact();
	}

	public String getUserIdFromJWT(String token) {
		Claims claims = Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody();
		System.out.println("+++++++++++ Expiration Date : " + claims.getExpiration());
		return claims.getSubject();
	}

	public boolean validateToken(String authToken) throws SignatureException, MalformedJwtException,
			ExpiredJwtException, UnsupportedJwtException, IllegalArgumentException {
		System.err.println("INSIDE VALIDATE TOKEN !");
		Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
		return true;
		/*
		 * } catch (SignatureException ex) { logger.error("Invalid JWT signature"); }
		 * catch (MalformedJwtException ex) { logger.error("Invalid JWT token"); } catch
		 * (ExpiredJwtException ex) { logger.error("Expired JWT token"); throw new
		 * org.springframework.security.access.AccessDeniedException("erer"); //throw
		 * new BadRequestException("HUSSEIN : EXPIRED TOKEN EXCETPTION"); } catch
		 * (UnsupportedJwtException ex) { logger.error("Unsupported JWT token"); } catch
		 * (IllegalArgumentException ex) { logger.error("JWT claims string is empty.");
		 * } return false;
		 */
	}
}
