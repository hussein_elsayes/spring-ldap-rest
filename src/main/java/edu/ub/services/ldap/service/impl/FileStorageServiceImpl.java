package edu.ub.services.ldap.service.impl;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import edu.ub.services.ldap.exceptions.BadOrFileNotFoundException;
import edu.ub.services.ldap.exceptions.FileStorageException;
import edu.ub.services.ldap.service.FileStorageService;

public class FileStorageServiceImpl implements FileStorageService {

	@Value("${file.upload-dir}")
	String fileUploadDir;

	@Override
	public String storeFile(MultipartFile file) throws IOException {
		// --------------------File-----------------------
		// validate file
		if (file == null || !(file.getContentType().equals("application/pdf"))) {
			throw new BadOrFileNotFoundException("Attach .pdf file");
		}
		
		// upload file
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());
			
		System.out.println("File Name : " + fileName);
		if (fileName.contains("..")) {
			throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
		}
		Path fileLocation = Paths.get(fileUploadDir).toAbsolutePath().normalize();
		
		File destinationFile = new File(fileLocation.toString());
		if(destinationFile.exists())
		{
			System.err.println("This File Exists !!");
		}
		System.out.println("File Location : " + fileName);
		Path targetLocation = fileLocation.resolve(fileName);
		
		if(new File(targetLocation.toString()).exists())
		{
			if(fileName.indexOf('.') > 0 )
			{
				fileName = fileName.substring(0, fileName.lastIndexOf('.'));
				fileName = fileName + "_0";
				fileName = fileName + ".pdf";
				System.err.println("New File Name After dealing with duplicate is : " + fileName);
				targetLocation = fileLocation.resolve(fileName);
				System.out.println("File Location after dealing with duplicate is  : " + targetLocation);
			}
		}
		
		
			Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
		
		System.out.println("File Uploaded to  : " + targetLocation);
		return fileName;
	}

	@Override
	public Resource loadFileAsResource(String fileName) throws MalformedURLException {
		Path filePath = Paths.get(fileUploadDir).resolve(fileName).normalize();
		System.out.println("File Path : " + filePath);
		Resource resource = new UrlResource(filePath.toUri());
		if (resource.exists()) {
			return resource;
		} else {
			System.err.println("Resurce Doesn't exist");
			return null;
		}
	}
}
