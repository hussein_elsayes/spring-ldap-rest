package edu.ub.services.ldap.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.List;
import java.util.Locale;

import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import edu.ub.services.common.RequestType;
import edu.ub.services.ldap.dto.LdapPerson;
import edu.ub.services.ldap.dto.RequestDetails;
import edu.ub.services.ldap.dto.UserRequest;

@Component
public interface UserService {
	public LdapPerson getLdapUserDetails(String samAccount);
	
	//create user request
	public long createUserRequest(RequestDetails details,MultipartFile file, Locale locale) throws IOException;
	//validate the request by vcode
	public UserRequest validateRequestByVcode(long id,String vcode);
	//deny the request from admin
	public UserRequest denyRequest(long reqId,Locale locale) throws UnsupportedEncodingException;
	//change attributes request
	public long changeAttrRequest(RequestDetails details, Locale locale);
	//change password request
	public long changePassRequest(String samAccount, Locale locale);
	//change password request
	public long resetPassRequest(String samAccount, Locale locale);
	//get the non validated requests for the user
	public List<UserRequest> getUserPendingRequests(String mobileNo,String nationalId,String employeeId);
	
	
	//create AD user
	public long createUser(long reqId, String samAccount);
	//create AD user password
	public long createUserPassword(long reqId, String password, Locale locale) throws UnsupportedEncodingException;
	//update user attributes	
	public long updateUserAttributes(long id, String vcode);
	//update user password
	public long updateUserPassword(long id, String vcode, String newPassword, String confirmPassword);
	//reset user password
	public long resetUserPassword(long id, String password, String confirmPassword, String vcode);
	
	
	
	//get attachment
	public Resource getRequestAttachment(long reqId) throws MalformedURLException;
	//resend vcode
	public void resendVcode(long reqId);

	
	//previously added a validated request
	public boolean previouslyAddedValidRequest(RequestDetails details);
	//previously added create Request
	public boolean previouslyAddedCreateRequest(RequestDetails details);
	//has reached maximum attempts by mobile
	public boolean hasMaximumRequestAttemptsByMobile(RequestDetails details);
	//has reached maximum attempts by samaccount
	public boolean hasMaximumRequestAttemptsBySamAccount(String samAccount);
	//has right directory values
	public boolean hasRightDirectoryValues(RequestDetails details);
	
	
}
