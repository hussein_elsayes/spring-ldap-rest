package edu.ub.services.ldap.service.impl;

import static org.springframework.ldap.query.LdapQueryBuilder.query;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.naming.InvalidNameException;
import javax.naming.Name;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.ldap.LdapName;
import javax.naming.ldap.Rdn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.filter.AndFilter;
import org.springframework.ldap.filter.EqualsFilter;
import org.springframework.ldap.support.LdapNameBuilder;

import com.mysql.jdbc.StringUtils;

import edu.ub.services.ldap.dto.Department;
import edu.ub.services.ldap.dto.LdapOU;
import edu.ub.services.ldap.dto.LdapPerson;
import edu.ub.services.ldap.exceptions.UserExistsException;
import edu.ub.services.ldap.repositories.DepartmentRepository;
import edu.ub.services.ldap.repositories.LdapUserRepository;
import edu.ub.services.ldap.service.LdapPersonService;

public class LdapPersonServiceImpl implements LdapPersonService {

	@Autowired
	LdapTemplate ldapTemplate;

	@Autowired
	LdapUserRepository myLdapRepository;

	@Autowired
	LdapPersonService personService;

	@Autowired
	DepartmentRepository depRepository;

	@Override
	public LdapPerson addPerson(LdapPerson person) throws UserExistsException {
		// check if user doesn't exists before creating it
		if (find(person.getSamAccount()) == null) {
			System.out.println("user doesn't exist trying to create the user ");
			Attributes attrs = LdapPerson.getAttributes(person);
			System.err.println(attrs);
			Name dn = person.getDn();
			System.err.println(dn);
			ldapTemplate.bind(dn, null, attrs);
			return person;
		} else {
			System.out.println("user exists , will not create the user");
			throw new UserExistsException();
		}
	}

	@Override
	public LdapPerson updatePerson(LdapPerson person) {
		try {
			moveToOu(person, person.getDepartment());
		} catch (InvalidNameException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ldapTemplate.update(person);		
		return person;
	}
	
	private LdapPerson moveToOu(LdapPerson person, String ou) throws InvalidNameException {
		List<LdapPerson> persons = ldapTemplate.find(query().where("sAMAccountName").is(person.getSamAccount()), LdapPerson.class);
		Name sourceOu = persons.get(0).getDn();
		System.err.println("THE SOURCE : " + sourceOu);
		
		// get the destination ou	 
		List<LdapOU> ous = ldapTemplate.find(query().where("ou").is(ou), LdapOU.class);
		if (ous != null && ous.size() != 0)
		{
			LdapOU theOu = ous.get(0);
			System.err.println(theOu.getDistinguishedName());
			//Name destinationDn = LdapNameBuilder.newInstance(theOu.getDistinguishedName()).build();
			Name destinationDn = theOu.getDn();
			destinationDn.add("CN="+ person.getSamAccount());
						
			System.err.println("THE DESTINATION : " + destinationDn);
			ldapTemplate.rename(sourceOu, destinationDn); 
		}else
		{
			System.err.println("NOT FOUND !");
		}
		return null;
	}

	@Override
	public LdapPerson find(String samAccount) {
		List<LdapPerson> persons = ldapTemplate.find(query().where("samAccountName").is(samAccount), LdapPerson.class);
		if (persons != null && persons.size() != 0) {
			LdapPerson person = persons.get(0);
			String literalDepartment = person.getDepartment();
			Department dep = depRepository.getDepartmentIdByLiteralValue(literalDepartment);
			if (dep != null) {
				person.setDepId(dep.getId());
				person.setOuId(dep.getOu().getId());
			}
			// set depId to ldap Person Object
			person.setMembership(getGroupMembership(person));
			return person;
		}
		return null;
	}

	@Override
	public String[] getGroupMembership(LdapPerson person) {
		DirContextOperations context = ldapTemplate.lookupContext(person.getDn());
		String[] attributes = context.getStringAttributes("memberOf");
		if (attributes != null && attributes.length != 0) {
			for (int i = 0; i < attributes.length; i++) {
				LdapName dn = LdapNameBuilder.newInstance(attributes[i]).build();
				for (Rdn rdn : dn.getRdns()) {
					if (rdn.getType().equalsIgnoreCase("CN")) {
						attributes[i] = rdn.getValue().toString();
					}
				}
			}
		}
		return attributes;
	}

	@Override
	public LdapPerson ResetPassword(LdapPerson person, String password) {
		System.out.println("updating password...\n");
		String quotedPassword = "\"" + password + "\"";
		char unicodePwd[] = quotedPassword.toCharArray();
		byte pwdArray[] = new byte[unicodePwd.length * 2];
		for (int i = 0; i < unicodePwd.length; i++) {
			pwdArray[i * 2 + 1] = (byte) (unicodePwd[i] >>> 8);
			pwdArray[i * 2 + 0] = (byte) (unicodePwd[i] & 0xff);
		}
		System.out.print("encoded password: ");
		for (int i = 0; i < pwdArray.length; i++) {
			System.out.print(pwdArray[i] + " ");
		}
		ModificationItem[] mods = new ModificationItem[2];
		mods[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute("UnicodePwd", pwdArray));
		mods[1] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute("userAccountControl", "66048"));
		ldapTemplate.modifyAttributes(person.getDn(), mods);
		return person;
	}



	@Override
	public boolean authenticate(String username, String password) {
		AndFilter filter = new AndFilter();
		filter.and(new EqualsFilter("objectclass", "person")).and(new EqualsFilter("sAmAccountName", username));
		return ldapTemplate.authenticate(LdapNameBuilder.newInstance().build(), filter.toString(), password);
	}

	@Override
	public boolean updatedAttributes(String samAccount) {
		LdapPerson person = find(samAccount);
		if (person != null) {
			if (StringUtils.isNullOrEmpty(person.getFirstName())) {
				return false;
			}
			if (StringUtils.isNullOrEmpty(person.getLastName())) {
				return false;
			}
			if (StringUtils.isNullOrEmpty(person.getArabicName())) {
				return false;
			}
			if (StringUtils.isNullOrEmpty(person.getMobile())) {
				return false;
			}
			/*
			 * IP Phone is not required if(StringUtils.isNullOrEmpty(person.getIpPhone())) {
			 * return false; }
			 */
			if (StringUtils.isNullOrEmpty(person.getDepartment())) {
				return false;
			}
			if (StringUtils.isNullOrEmpty(person.getEmployeeID())) {
				return false;
			}
			if (StringUtils.isNullOrEmpty(person.getNationalId())) {
				return false;
			}
			if (StringUtils.isNullOrEmpty(person.getGender())) {
				return false;
			}
			if (StringUtils.isNullOrEmpty(person.getEnglishName())) {
				return false;
			}
			return true;
		}
		return false;
	}

	@Override
	public boolean authenticate(String samAccount, String employeeId, String mobileNo) {
		LdapPerson person = find(samAccount);
		if (person != null) {
			if (samAccount.equals(person.getSamAccount()) && employeeId.endsWith(person.getEmployeeID())
					&& mobileNo.equals(person.getMobile())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String findOu(String ou) {
		List<LdapOU> ous = ldapTemplate.find(query().where("ou").is(ou), LdapOU.class);
		if (ous != null && ous.size() != 0) {
			LdapOU theOu = ous.get(0);
			System.err.println(theOu.getDistinguishedName());
		} else {
			System.err.println("NOT FOUND !");
		}
		return null;
	}

}
