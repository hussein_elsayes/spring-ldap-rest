package edu.ub.services.ldap.service;

import java.io.UnsupportedEncodingException;
import java.util.Locale;

public interface SmsService {

	public boolean sendVcode(String message);
	
	public void sendNewPassword();
	
	public int sendVcodeMessage(String mobileNo,Locale locale) throws UnsupportedEncodingException ;
	
	public void sendTextMessage(String mobileNo,String message)throws UnsupportedEncodingException ; 
}
