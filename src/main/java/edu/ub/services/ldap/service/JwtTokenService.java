package edu.ub.services.ldap.service;

public interface JwtTokenService {
	public String generateToken(String samAccount);
	public String getUserIdFromJWT(String token);
	public boolean validateToken(String authToken);
}
