package edu.ub.services.ldap.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import edu.ub.services.ldap.dto.CustomUserDetails;
import edu.ub.services.ldap.dto.LdapPerson;
import edu.ub.services.ldap.service.LdapPersonService;

public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	LdapPersonService personService;
	
	@Override
	public UserDetails loadUserByUsername(String principalName) throws UsernameNotFoundException {

		LdapPerson person = personService.find(principalName);
		//bake an object the implements UserDetails interface
		UserDetails details = create(person);
		return details;
	}
	
	private CustomUserDetails create(LdapPerson person) {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		if(person.getMembership() != null)
		{
			for(int i=0;i<person.getMembership().length;i++)
			{
				authorities.add(new SimpleGrantedAuthority("ROLE_" +person.getMembership()[i].toUpperCase()));
			}
		}
		return new CustomUserDetails(person.getSamAccount(), person.getFirstName(),"", authorities);
	}

}
