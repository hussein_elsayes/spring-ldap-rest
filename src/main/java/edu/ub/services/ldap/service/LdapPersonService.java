package edu.ub.services.ldap.service;

import org.springframework.stereotype.Service;

import edu.ub.services.ldap.dto.LdapPerson;
import edu.ub.services.ldap.exceptions.UserExistsException;

@Service
public interface LdapPersonService {

	//-----------------------AUTHENTICATE------------------------
	public boolean authenticate(String username,String password);
	public boolean authenticate(String samAccount, String employeeId, String mobileNo);

	//-----------------------CRUD--------------------------------
	//CREATE PERSON
	public LdapPerson addPerson(LdapPerson person) throws UserExistsException;
	//UPDATE
	public LdapPerson updatePerson(LdapPerson person);
	//READ
	public LdapPerson find(String samAccount);
	//RESET PASSWORD
	public LdapPerson ResetPassword(LdapPerson person,String password);
	//CHECK UPDATED ATTRIBUTES
	public boolean updatedAttributes(String samAccount);
	
	//-----------------------OPERATIONS------------------------
	//GET GROUPS
	public String[] getGroupMembership(LdapPerson person);
	
	public String findOu(String ou);

}
