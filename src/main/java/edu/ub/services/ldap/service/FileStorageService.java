package edu.ub.services.ldap.service;

import java.io.IOException;
import java.net.MalformedURLException;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface FileStorageService {
	public String storeFile(MultipartFile file) throws IOException;
	public Resource loadFileAsResource(String fileName) throws MalformedURLException;
}
