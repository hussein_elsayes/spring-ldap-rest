package edu.ub.services.ldap.service.impl;

import java.io.UnsupportedEncodingException;
import java.text.MessageFormat;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import edu.ub.services.ldap.service.SmsService;

public class SmsServiceImpl implements SmsService {

	@Autowired
	Mobily mobily;
	
	@Autowired
	MessageSource messageSource;

	@Override
	public boolean sendVcode(String message) {
		return false;
	}

	public static int generateCode() {
		return (int) Math.round(Math.random() * 89999) + 10000;
	}

	@Override
	public void sendNewPassword() {
		// TODO Auto-generated method stub

	}

	@Override
	public int sendVcodeMessage(String mobileNo, Locale locale) throws UnsupportedEncodingException {
		int generatedCode = (int) Math.round(Math.random() * 89999) + 10000;
		System.out.println("++++++++++++++++GENERATED VCODE : " + generatedCode + "++++++++++++++++");
		//sendMessage(String.format(messageSource.getMessage("sms.messages.vcode", null,locale), generatedCode), mobileNo); //"966533800945"
		return generatedCode;
	}

	private void sendMessage(String message,String mobileNo) throws UnsupportedEncodingException {
		mobily.sendMessage("966507973703", "M@b$ws8", "IT-UB", message, mobileNo);
	}

	@Override
	public void sendTextMessage(String mobileNo, String message) throws UnsupportedEncodingException {
		// TODO Auto-generated method stub
		sendMessage(message, mobileNo);
	}

}
