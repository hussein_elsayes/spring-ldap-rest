package edu.ub.services.ldap.service.impl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import edu.ub.services.common.RequestStatus;
import edu.ub.services.common.RequestType;
import edu.ub.services.common.UserDetailsFactory;
import edu.ub.services.ldap.dto.Attachment;
import edu.ub.services.ldap.dto.LdapPerson;
import edu.ub.services.ldap.dto.RequestDetails;
import edu.ub.services.ldap.dto.UserRequest;
import edu.ub.services.ldap.dto.Vcode;
import edu.ub.services.ldap.exceptions.BadRequestTypeException;
import edu.ub.services.ldap.exceptions.InvalidCodeException;
import edu.ub.services.ldap.exceptions.PasswordMatchException;
import edu.ub.services.ldap.exceptions.RequestNotFoundException;
import edu.ub.services.ldap.exceptions.UserCreationException;
import edu.ub.services.ldap.exceptions.UserExistsException;
import edu.ub.services.ldap.exceptions.UserNotFoundException;
import edu.ub.services.ldap.repositories.RequestRepository;
import edu.ub.services.ldap.repositories.UserDetailsRepository;
import edu.ub.services.ldap.repositories.VcodeRepository;
import edu.ub.services.ldap.response.ApiResponse;
import edu.ub.services.ldap.service.FileStorageService;
import edu.ub.services.ldap.service.LdapPersonService;
import edu.ub.services.ldap.service.SmsService;
import edu.ub.services.ldap.service.UserService;

public class UserServiceImpl implements UserService {

	@Autowired
	RequestRepository requestRepo;

	@Autowired
	VcodeRepository vCodeRepo;

	@Autowired
	UserDetailsRepository detailsRepo;

	@Autowired
	LdapPersonService ldapService;

	@Autowired
	UserDetailsFactory userDetailsFactory;

	@Autowired
	FileStorageService fileStorageService;

	@Value("${file.upload-dir}")
	String fileUploadDir;

	@Autowired
	SmsService smsService;

	@Autowired
	MessageSource messageSource;

	// -------------------------------------------------------------- Requests
	// --------------------------------------------------------------------------------
	@Override
	public LdapPerson getLdapUserDetails(String samAccount) {
		UserDetailsFactory factory = new UserDetailsFactory();
		return ldapService.find(samAccount);
	}

	@Override
	public long createUserRequest(RequestDetails details, MultipartFile file, Locale locale) throws IOException {

		// upload file
		String fileName = fileStorageService.storeFile(file);
		// create Attachment object
		Attachment attch = new Attachment();
		attch.setFileName(fileName);
		// create request object
		UserRequest req = new UserRequest();
		req.setReqStatus(RequestStatus.WAITING_VALIDATION.getValue());
		req.setReqType(RequestType.CREATE_USER.getValue());
		req.setAttachment(attch);
		req.setReqDetails(details);
		// create Vcode Object with attachment and request
		Vcode vcode = new Vcode();
		int gerenatedVcode;
		try {
			gerenatedVcode = smsService.sendVcodeMessage(details.getMobile(), locale);
			vcode.setCodeBody(String.valueOf(gerenatedVcode));
			vcode.setUserRequest(req);
			vcode = vCodeRepo.save(vcode);
			return vcode.getId();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // sendVcode();
		return 0;
	}

	@Override
	public UserRequest validateRequestByVcode(long id, String vcode) {
		Vcode code = vCodeRepo.findById(id).get();
		if (code != null & code.getCodeBody().equals(vcode)) {
			// validate request and send it back
			long reqId = code.getUserRequest().getId();
			UserRequest request = requestRepo.findById(reqId).get();
			request.setReqStatus(RequestStatus.VALIDATED.getValue());
			return requestRepo.save(request);
		} else {
			return null;
		}
	}

	@Override
	public long changeAttrRequest(RequestDetails details, Locale locale) {
		UserRequest req = new UserRequest(RequestType.CHANGE_ATTRIBUTES.getValue(),
				RequestStatus.WAITING_VALIDATION.getValue(), details);
		// create Vcode Object
		Vcode vcode = new Vcode();
		int gerenatedVcode;
		try {
			gerenatedVcode = smsService.sendVcodeMessage(details.getMobile(), locale);
			vcode.setCodeBody(String.valueOf(gerenatedVcode));
			vcode.setUserRequest(req);
			vcode = vCodeRepo.save(vcode);
			return vcode.getId();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public long changePassRequest(String samAccount, Locale locale) {
		LdapPerson person = ldapService.find(samAccount);
		// req type = 3 (changePass)
		RequestDetails details = new RequestDetails();
		details = userDetailsFactory.create(person);
		UserRequest req = new UserRequest(RequestType.CHANGE_PASSWORD.getValue(),
				RequestStatus.WAITING_VALIDATION.getValue(), details);
		Vcode vcode = new Vcode();
		int gerenatedVcode;
		try {
			gerenatedVcode = smsService.sendVcodeMessage(details.getMobile(), locale);
			vcode.setCodeBody(String.valueOf(gerenatedVcode));
			vcode.setUserRequest(req);
			vcode = vCodeRepo.save(vcode);
			return vcode.getId();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public long resetPassRequest(String samAccount, Locale locale) {
		LdapPerson person = ldapService.find(samAccount);
		// req type = 3 (changePass)
		RequestDetails details = new RequestDetails();
		details = userDetailsFactory.create(person);
		UserRequest req = new UserRequest(RequestType.RESET_PASSWORD.getValue(),
				RequestStatus.WAITING_VALIDATION.getValue(), details);
		Vcode vcode = new Vcode();
		int gerenatedVcode;
		try {
			gerenatedVcode = smsService.sendVcodeMessage(details.getMobile(), locale);
			vcode.setCodeBody(String.valueOf(gerenatedVcode));
			vcode.setUserRequest(req);
			vcode = vCodeRepo.save(vcode);
			return vcode.getId();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public List<UserRequest> getUserPendingRequests(String mobileNo, String nationalId, String employeeId) {
		List<UserRequest> pendingRequests = requestRepo.getNonValidatedRequests(mobileNo, nationalId, employeeId);
		return pendingRequests;
	}

	@Override
	public Resource getRequestAttachment(long reqId) throws MalformedURLException {
		String fileName = requestRepo.findById(reqId).get().getAttachment().getFileName();
		System.err.println(fileName);
		// call service to search for the file
		Resource resource = fileStorageService.loadFileAsResource(fileName);
		System.err.println("Resource Filename is : " + resource.getFilename());
		return resource;
	}

	@Override
	public void resendVcode(long reqId) {
		Vcode vcode = vCodeRepo.findById(reqId).get();
		String vcodeBody = String.valueOf(sendVcode());
		vcode.setCodeBody(vcodeBody);
		vCodeRepo.save(vcode);
		smsService.sendVcode(vcodeBody);
	}

	@Override
	public UserRequest denyRequest(long reqId, Locale locale) throws UnsupportedEncodingException {
		UserRequest request = requestRepo.findById(reqId).get();
		if (request != null) {
			
				// set request to be denied (status = 3)
				request.setReqStatus(RequestStatus.DENIED.getValue());
				requestRepo.save(request);
				// SEND MESSAGE TO USER TELL HIM THAT THE REQUESWT IS DENIED
				smsService.sendTextMessage(request.getReqDetails().getMobile(),
						messageSource.getMessage("sms.messages.requests.denied", null, locale));
			
			return request;
		} else {
			return null;
		}

	}

	// -------------------------------------------------------------- Confirmations
	// --------------------------------------------------------------------------------

	@Override
	public long createUser(long reqId, String samAccount) {
		UserRequest request = requestRepo.findById(reqId).get();
		if (request != null) {
			// set request to be completed ( status = 4)
			request.setReqStatus(RequestStatus.CONFIRMED.getValue());
			// set sam account
			request.getReqDetails().setSamAccount(samAccount);
			requestRepo.save(request);
			LdapPerson ldapPerson = LdapPerson.create(request);
			if (ldapService.find(samAccount) != null) // If user exitsts Throw Error
			{
				throw new UserExistsException();
			} else // save person to AD
			{
				if (ldapService.addPerson(ldapPerson) != null) {
					return reqId;
				} else {
					throw new UserCreationException();
				}
			}

		} else {
			throw new RequestNotFoundException();
		}
	}

	@Override
	public long createUserPassword(long reqId, String password, Locale locale) throws UnsupportedEncodingException {
		UserRequest request = requestRepo.findById(reqId).get();
		if (request != null) {
			String samAcc = request.getReqDetails().getSamAccount();
			LdapPerson person = ldapService.find(samAcc);
			if (person != null) {
				System.err.println(person.getDn().toString());

				// SEND SMS WITH CREATION SUCCESSFUL INCLUDING NEW USERNAME AND PASSWORD
				ldapService.ResetPassword(person, password);
				smsService.sendTextMessage(request.getReqDetails().getMobile(), String.format(
						messageSource.getMessage("sms.messages.create.request.success", null, locale), samAcc, password));

				return reqId;
			} else {
				System.err.println("no user found _ NULL");
				throw new UserNotFoundException();

			}
		} else {
			throw new RequestNotFoundException();
		}
	}

	@Override
	public long updateUserAttributes(long id, String vcode) {
		Vcode code = vCodeRepo.findById(id).get();
		if (code != null & code.getCodeBody().equals(vcode)) {
			System.out.println("Code is valid !!");
			long reqId = code.getUserRequest().getId();
			UserRequest request = requestRepo.findById(reqId).get();
			LdapPerson person;
			if (request.getReqType() == RequestType.CHANGE_ATTRIBUTES.getValue()) {
				request.setReqStatus(RequestStatus.CONFIRMED.getValue());
				requestRepo.save(request);
				// ldap update attributes
				person = LdapPerson.create(request);
				ldapService.updatePerson(person);
				return reqId;
			} else {
				throw new BadRequestTypeException();
			}

		} else {
			System.out.println("Code is invalid !!");
			throw new InvalidCodeException();
		}
	}

	@Override
	public long updateUserPassword(long id, String vcode, String newPassword, String confirmPassword) {
		Vcode code = vCodeRepo.findById(id).get();
		if (code != null & code.getCodeBody().equals(vcode)) {

			if (newPassword.equals(confirmPassword)) {
				long reqId = code.getUserRequest().getId();
				UserRequest request = requestRepo.findById(reqId).get();

				if (request != null) {
					if (request.getReqType() == RequestType.CHANGE_PASSWORD.getValue()) {
						request.setReqStatus(RequestStatus.CONFIRMED.getValue());
						requestRepo.save(request);
						// reset password in AD
						String samAcc = request.getReqDetails().getSamAccount();
						LdapPerson person = ldapService.find(samAcc);
						if (person != null) {
							System.err.println(person.getDn().toString());
							ldapService.ResetPassword(person, newPassword);
							return reqId;
						} else {
							System.err.println("no user found _ NULL");
							throw new UserNotFoundException();
						}
					} else {
						throw new BadRequestTypeException();
					}

				} else {
					throw new RequestNotFoundException();
				}
			} else {
				throw new PasswordMatchException();
			}
		} else {
			System.out.println("Code is invalid !!");
			throw new InvalidCodeException();
		}
	}

	@Override
	public long resetUserPassword(long id, String password, String confirmPassword, String vcode) {
		Vcode code = vCodeRepo.findById(id).get();
		if (code != null & code.getCodeBody().equals(vcode)) {

			if (password.equals(confirmPassword)) {

				long reqId = code.getUserRequest().getId();
				UserRequest request = requestRepo.findById(reqId).get();

				if (request != null) {
					if (request.getReqType() == 4) {
						request.setReqStatus(4);
						requestRepo.save(request);
						// reset password in AD
						String samAcc = request.getReqDetails().getSamAccount();
						LdapPerson person = ldapService.find(samAcc);
						if (person != null) {
							System.err.println(person.getDn().toString());
							ldapService.ResetPassword(person, password);
							return reqId;
						} else {
							System.err.println("no user found _ NULL");
							throw new UserNotFoundException();
						}
					} else {
						throw new BadRequestTypeException();
					}

				} else {
					throw new RequestNotFoundException();
				}
			} else {
				throw new PasswordMatchException();
			}
		} else {
			System.out.println("Code is invalid !!");
			throw new InvalidCodeException();
		}
	}

	// ---------------UTILITY -------------------------------------------------

	private int sendVcode() {
		int generatedCode = (int) Math.round(Math.random() * 89999) + 10000;
		System.out.println("++++++++++++++++GENERATED VCODE : " + generatedCode + "++++++++++++++++");
		return generatedCode;
	}

	// --------------- Constraints -------------------------------------------------
	@Override
	public boolean previouslyAddedValidRequest(RequestDetails details) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean previouslyAddedCreateRequest(RequestDetails details) {
		int numberOfRequests = requestRepo.getRequestsByMobileNumber(details.getMobile());
		if (numberOfRequests > 0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean hasMaximumRequestAttemptsByMobile(RequestDetails details) {
		int numberOfRequests = requestRepo.getRequestsPerDayByMobile(details.getMobile());
		System.out.println("number of requests : " + numberOfRequests);
		if (numberOfRequests >= 3) {
			System.out.println("Maximum Requests Reached !!");
			return true;
		}
		return false;
	}

	@Override
	public boolean hasMaximumRequestAttemptsBySamAccount(String samAccount) {
		int numberOfRequests = requestRepo.getRequestsPerDayBySamAccount(samAccount);
		System.out.println("number of requests : " + numberOfRequests);
		if (numberOfRequests >= 3) {
			System.out.println("Maximum Requests Reached !!");
			return true;
		}
		return false;
	}

	@Override
	public boolean hasRightDirectoryValues(RequestDetails details) {
		// TODO Auto-generated method stub
		return false;
	}

}
