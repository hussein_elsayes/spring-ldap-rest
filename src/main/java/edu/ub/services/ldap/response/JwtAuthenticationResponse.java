package edu.ub.services.ldap.response;

public class JwtAuthenticationResponse {
	
	private String token;
	private String role;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
	public String getRole() {
		return role;
	}
	
	public void setRole(String role) {
		this.role = role;
	}

	public JwtAuthenticationResponse(String token,String role) {
		this.role = role;
		this.token = token;
	}
	
	
}
