package edu.ub.services.ldap.response;

import edu.ub.services.ldap.dto.errors.ValidationErrorWrapper;

public class ValidationErrorResponse {

	private int status;
	private ValidationErrorWrapper errorsWrapper;
	private long timestamp;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public ValidationErrorWrapper getErrorsWrapper() {
		return errorsWrapper;
	}

	public void setErrorsWrapper(ValidationErrorWrapper errorsWrapper) {
		this.errorsWrapper = errorsWrapper;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

}
