package edu.ub.services.ldap.response;

public class UploadFileResponse {
	private int status;
	private String fileName;
	private String requestId;
		
	public UploadFileResponse()
	{
		
	}
	
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public UploadFileResponse(int status,String fileName, String requestId) {
		this.status = status;
		this.fileName = fileName;
		this.requestId = requestId;
	}

}
