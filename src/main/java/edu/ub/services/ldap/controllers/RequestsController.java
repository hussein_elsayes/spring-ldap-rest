package edu.ub.services.ldap.controllers;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.annotation.security.RolesAllowed;
import javax.validation.ConstraintViolation;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.BindingResult;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

import edu.ub.services.common.ErrorsWrapperFactory;
import edu.ub.services.common.UserDetailsFactory;
import edu.ub.services.ldap.dto.Department;
import edu.ub.services.ldap.dto.RequestDetails;
import edu.ub.services.ldap.dto.UserRequest;
import edu.ub.services.ldap.exceptions.RequestDetailsValidationException;
import edu.ub.services.ldap.repositories.DepartmentRepository;
import edu.ub.services.ldap.repositories.RequestRepository;
import edu.ub.services.ldap.repositories.VcodeRepository;
import edu.ub.services.ldap.response.ApiResponse;
import edu.ub.services.ldap.service.FileStorageService;
import edu.ub.services.ldap.service.LdapPersonService;
import edu.ub.services.ldap.service.UserService;

@RestController
@RequestMapping("/api/requests")

public class RequestsController {

	@Autowired
	RequestRepository requestRepo;

	@Autowired
	VcodeRepository vcodeRepo;

	@Autowired
	LdapPersonService ldapUserService;

	@Autowired
	UserService userService;

	@Autowired
	LocalValidatorFactoryBean validator;

	@Autowired
	DepartmentRepository depRepo;

	@Autowired
	VcodeRepository vCodeRepo;

	@Autowired
	UserDetailsFactory userDetailsFactory;

	@Autowired
	MessageSource messageSource;

	@Autowired
	FileStorageService fileStorageService;

	/*
	 * @GetMapping("/") //@RolesAllowed("ROLE_EMPADM") public List<UserRequest>
	 * getAll(@AuthenticationPrincipal final UserDetails userDetails) {
	 * System.out.println("USERNAME FROM PRINCIPAL : " + userDetails.getUsername());
	 * 
	 * 
	 * 
	 * for (GrantedAuthority authority : userDetails.getAuthorities()) {
	 * System.err.println("Authority From Requests Controller  : " +
	 * authority.getAuthority()); } return requestRepo.findAll(); }
	 */

	@RolesAllowed("ROLE_EMPADM")
	@GetMapping("/auth")
	public List<UserRequest> getCreateUserRequests() {
		// return only the validated create requests
		return requestRepo.getWaitingConfirmationCreateUserRequests();
	}

	@RolesAllowed("ROLE_EMPADM")
	@GetMapping("/auth/{id}")
	public UserRequest getRequest(@PathVariable long id) {
		return requestRepo.findById(id).get();
	}

	@PostMapping("/createUser")
	public ResponseEntity<?> addUserRequestWithAttachment(@RequestParam("attachment") MultipartFile file,
			@RequestParam("userDetailsJson") String reqDetailsJson, Locale locale) throws IOException {
		// PARSE & VALIDATE USERDETAILS
		RequestDetails details = null;
		details = new ObjectMapper().readValue(reqDetailsJson, RequestDetails.class);
		System.err.println("Details = " + details);

		// check for constraints
		if (!userService.hasMaximumRequestAttemptsByMobile(details)) {
			if (!userService.previouslyAddedCreateRequest(details)) {
				// Validate
				Set<ConstraintViolation<RequestDetails>> result = validator.validate(details);
				if (!result.isEmpty()) {
					throw new RequestDetailsValidationException(
							messageSource.getMessage("requests.invalid.data", null, locale),
							ErrorsWrapperFactory.create(result));
				}

				// set department
				Department department = new Department();
				department.setId(details.getDepId());
				details.setDepartment(department);

				// ADD REQUEST TO DATABASE
				long VCodeId = userService.createUserRequest(details, file, locale);
				return ResponseEntity.ok(new ApiResponse(true, String.valueOf(VCodeId)));
			} else {
				return ResponseEntity.badRequest().body(
						(new ApiResponse(false, messageSource.getMessage("requests.mobile.exists", null, locale))));
			}
		} else {
			return ResponseEntity.badRequest()
					.body((new ApiResponse(false, messageSource.getMessage("requests.maximum.reached", null, locale))));

		}
	}

	@PostMapping("/validateCreateUser")
	public ResponseEntity<?> validate(@RequestParam("id") long id, @RequestParam("vcode") String vcode, Locale locale) {
		if (userService.validateRequestByVcode(id, vcode) != null) {
			return ResponseEntity
					.ok(new ApiResponse(true, messageSource.getMessage("requests.validate.success", null, locale)));
		} else {
			return ResponseEntity.badRequest()
					.body((new ApiResponse(false, messageSource.getMessage("requests.validate.error", null, locale))));
		}
	}

	@RolesAllowed("ROLE_EMPADM")
	@PostMapping("/auth/deny")
	public ResponseEntity<?> deny(@RequestParam("reqId") long reqId, Locale locale) throws UnsupportedEncodingException {
		if (userService.denyRequest(reqId, locale) != null) {
			return ResponseEntity.ok(new ApiResponse(true, String.valueOf(reqId)));
		} else {
			return new ResponseEntity<ApiResponse>(
					new ApiResponse(false, messageSource.getMessage("requests.not.found", null, locale)),
					HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping("/auth/changeAttr")
	public ResponseEntity<?> changeAttributesRequest(@AuthenticationPrincipal final UserDetails userDetails,
			@RequestBody @Valid RequestDetails details, BindingResult bindingResult, Locale locale) {
		// check valid data
		if (bindingResult.hasErrors()) {
			throw new RequestDetailsValidationException(messageSource.getMessage("requests.invalid.data", null, locale),
					ErrorsWrapperFactory.create(bindingResult));
		}

		// check for constraints
		if (!userService.hasMaximumRequestAttemptsBySamAccount(details.getSamAccount())) {

			// set department
			Department department = new Department();
			department.setId(details.getDepId());
			details.setDepartment(department);

			// persist values
			String samAccount = userDetails.getUsername();
			details.setSamAccount(samAccount);
			long VCodeId = userService.changeAttrRequest(details, locale);
			return ResponseEntity.ok(new ApiResponse(true, String.valueOf(VCodeId)));
		} else {
			return ResponseEntity.badRequest()
					.body((new ApiResponse(false, messageSource.getMessage("requests.maximum.reached", null, locale))));
		}
	}

	@PostMapping("/auth/changePass")
	public ResponseEntity<?> changePassRequest(@AuthenticationPrincipal final UserDetails userDetails, Locale locale) {
		String samAccount = userDetails.getUsername();
		if (ldapUserService.updatedAttributes(samAccount)) {
			// check for constraints
			if (!userService.hasMaximumRequestAttemptsBySamAccount(samAccount)) {

				long vcodeId = userService.changePassRequest(samAccount, locale);
				return ResponseEntity.ok(new ApiResponse(true, String.valueOf(vcodeId)));
			} else {
				return ResponseEntity.badRequest()
						.body((new ApiResponse(false, messageSource.getMessage("requests.maximum.reached", null, locale))));
			}
		} else {
			return new ResponseEntity<ApiResponse>(
					new ApiResponse(false, messageSource.getMessage("requests.update.info.first", null, locale)),
					HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping("/resetPass")
	public ResponseEntity<?> resetPassRequest(@RequestParam("samAccount") String samAccount,
			@RequestParam("employeeId") String employeeId, @RequestParam("mobileNo") String mobileNo, Locale locale) {
		if (ldapUserService.authenticate(samAccount, employeeId, mobileNo)) {
			if (!userService.hasMaximumRequestAttemptsBySamAccount(samAccount)) {
				long vcodeId = userService.resetPassRequest(samAccount, locale);
				return ResponseEntity.ok(new ApiResponse(true, String.valueOf(vcodeId)));
			} else {
				return ResponseEntity.badRequest()
						.body((new ApiResponse(false, messageSource.getMessage("requests.maximum.reached", null, locale))));
			}
		} else {
			return new ResponseEntity<ApiResponse>(
					new ApiResponse(false, messageSource.getMessage("requests.invalid.data", null, locale)),
					HttpStatus.BAD_REQUEST);
		}
	}

	@RolesAllowed("ROLE_EMPADM")
	@PostMapping("/auth/downloadAttachment") // needs to be /auth/getFile/{redId} SECURED WITH ADMIN RIGHTS
	public ResponseEntity<?> getAttachment(@RequestParam long reqId) throws MalformedURLException {
		Resource resource = userService.getRequestAttachment(reqId);
		return ResponseEntity.ok().contentType(MediaType.parseMediaType("application/pdf"))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
				.body(resource);
	}

	@PostMapping("/pending")
	public ResponseEntity<?> testPending(@RequestParam("mobileNo") String mobileNo,
			@RequestParam("nationalId") String nationalId, @RequestParam("employeeId") String employeeId,
			Locale locale) {
		List<UserRequest> pendingRequests = userService.getUserPendingRequests(mobileNo, nationalId, employeeId);
		if (pendingRequests.size() > 0) {
			long reqId = pendingRequests.get(0).getId();
			userService.resendVcode(reqId);
			return new ResponseEntity<ApiResponse>(new ApiResponse(true, String.valueOf(reqId)), HttpStatus.OK);
		} else {
			return new ResponseEntity<ApiResponse>(
					new ApiResponse(false, messageSource.getMessage("requests.pending.not.found", null, locale)),
					HttpStatus.BAD_REQUEST);
		}
	}

}
