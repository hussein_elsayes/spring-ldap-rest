package edu.ub.services.ldap.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.ub.services.ldap.dto.Vcode;
import edu.ub.services.ldap.repositories.VcodeRepository;

@RestController
@RequestMapping("/api/vcode")
public class VcodeController {

	@Autowired
	VcodeRepository vcodeRepo;
	
	@GetMapping("/")
	public List<Vcode> getAll()
	{
		return vcodeRepo.findAll();
	}
	
}
