package edu.ub.services.ldap.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.ub.services.ldap.dto.Department;
import edu.ub.services.ldap.dto.OrganizationalUnit;
import edu.ub.services.ldap.repositories.DepartmentRepository;
import edu.ub.services.ldap.repositories.OURepository;
import edu.ub.services.ldap.response.ApiResponse;
import edu.ub.services.ldap.response.KeyValueResponse;

@RestController
@RequestMapping("/api")
public class OUController {

	@Autowired
	OURepository ouRepo;
	
	@Autowired
	DepartmentRepository depRepo;

	@GetMapping("/ous")
	public ResponseEntity<?>  getOus(@RequestHeader("Accept-Language") String locale) {
		List<KeyValueResponse> jsonOu = new ArrayList<KeyValueResponse>();
		for (OrganizationalUnit ou : ouRepo.findAll()) {
			if (locale.equals("ar-SA")) {
				jsonOu.add(new KeyValueResponse(String.valueOf(ou.getId()), ou.getOuNameAr()));
			} else {
				jsonOu.add(new KeyValueResponse(String.valueOf(ou.getId()), ou.getOuName()));
			}
		}
		return ResponseEntity.ok(jsonOu);
		//return ouRepo.findAll();
	}
	
	@GetMapping("/ous/{id}")
	public ResponseEntity<?> getOuDepartments(@PathVariable("id") long id,@RequestHeader("Accept-Language") String locale)
	{
		List<KeyValueResponse> jsonDepartments = new ArrayList<KeyValueResponse>();
		for(Department dep : depRepo.getDepartmentsByOuId(id))
		{
			if(locale.equals("ar-SA"))
			{
				jsonDepartments.add(new KeyValueResponse(String.valueOf(dep.getId()), dep.getDepName_ar()));
			}else
			{
				jsonDepartments.add(new KeyValueResponse(String.valueOf(dep.getId()), dep.getDepName()));
			}
		}			
		return ResponseEntity.ok(jsonDepartments);
	}
	

}
