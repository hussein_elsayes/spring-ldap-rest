package edu.ub.services.ldap.controllers;

import java.util.ArrayList;
import java.util.Locale;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.ub.services.ldap.dto.ViewLoginRequest;
import edu.ub.services.ldap.response.ApiResponse;
import edu.ub.services.ldap.response.JwtAuthenticationResponse;
import edu.ub.services.ldap.service.JwtTokenService;
import edu.ub.services.ldap.service.LdapPersonService;

@RestController
@RequestMapping("/api")
public class LoginController {
	
	@Autowired
	LdapPersonService personService;
	
	@Autowired
	@Qualifier("authenticationManagerBean")
    AuthenticationManager authenticationManager;
	
	@Autowired
	JwtTokenService jwtTokenService;
	
	@Autowired
	MessageSource messageSource;
	
	
	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody ViewLoginRequest loginRequest, Locale locale) 
	{
		System.out.println("Username : " + loginRequest.getUsername()+ ", Password : " + loginRequest.getPassword());
		
		Authentication authentication = authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword(), new ArrayList<>()));
		
		if(authentication != null)
		{
			String role= "";
			System.err.println("AUTHENTICATION RETURNED FROM LOGIN IS : " + authentication);
			for(GrantedAuthority auth : authentication.getAuthorities())
			{	if(auth.getAuthority().equals("ROLE_EMPADM")) {
				role = auth.getAuthority();
				}
			}
			String jwt = jwtTokenService.generateToken(authentication.getName());
			return ResponseEntity.ok(new JwtAuthenticationResponse(jwt,role));
		}else
		{
			return new ResponseEntity<>(new ApiResponse(false, messageSource.getMessage("login.invalid.credentials", null, locale)),HttpStatus.FORBIDDEN);
		}
	}
}
