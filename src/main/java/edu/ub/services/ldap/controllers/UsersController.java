package edu.ub.services.ldap.controllers;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Locale;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import edu.ub.services.common.RequestStatus;
import edu.ub.services.ldap.dto.Department;
import edu.ub.services.ldap.dto.LdapPerson;
import edu.ub.services.ldap.dto.RequestDetails;
import edu.ub.services.ldap.dto.UserRequest;
import edu.ub.services.ldap.dto.Vcode;
import edu.ub.services.ldap.dto.errors.FieldValidationError;
import edu.ub.services.ldap.dto.errors.ValidationErrorWrapper;
import edu.ub.services.ldap.exceptions.RequestDetailsValidationException;
import edu.ub.services.ldap.exceptions.UserExistsException;
import edu.ub.services.ldap.repositories.DepartmentRepository;
import edu.ub.services.ldap.repositories.RequestRepository;
import edu.ub.services.ldap.repositories.VcodeRepository;
import edu.ub.services.ldap.response.ApiResponse;
import edu.ub.services.ldap.service.LdapPersonService;
import edu.ub.services.ldap.service.UserService;

@RestController
@RequestMapping("/api/users")

public class UsersController {
	@Autowired
	UserService userService;

	@Autowired
	LocalValidatorFactoryBean validator;

	@Autowired
	VcodeRepository vcodeRepo;

	@Autowired
	DepartmentRepository depRepo;

	@Autowired
	RequestRepository requestRepo;

	@Autowired
	LdapPersonService ldapPersonService;

	@Autowired
	MessageSource messageSource;

	@GetMapping("/auth/")
	public LdapPerson getUser(@AuthenticationPrincipal final UserDetails userDetails) {
		String samAccount = userDetails.getUsername();
		return userService.getLdapUserDetails(samAccount);
	}

	@PostMapping("/auth/create")
	public ResponseEntity<?> confirmCreate(@RequestParam("reqId") long reqId,
			@RequestParam("samaccount") String samaccount, Locale locale) {
		return ResponseEntity.ok(new ApiResponse(true, String.valueOf(userService.createUser(reqId, samaccount))));
	}

	@PostMapping("/auth/createPass")
	public ResponseEntity<?> createPassword(@RequestParam("reqId") long reqId,
			@RequestParam("password") String password, Locale locale) throws UnsupportedEncodingException {
		return ResponseEntity
				.ok(new ApiResponse(true, String.valueOf(userService.createUserPassword(reqId, password,locale))));
	}

	@PostMapping("/auth/changeAttr")
	public ResponseEntity<?> confirmChangeAttributes(@RequestParam("id") long id, @RequestParam("vcode") String vcode,
			Locale locale) {
		userService.updateUserAttributes(id, vcode);
		return ResponseEntity
				.ok(new ApiResponse(true, String.valueOf(userService.updateUserAttributes(id, vcode))));
	}

	@PostMapping("/auth/changePass")
	public ResponseEntity<?> confirmChangePassword(@RequestParam("id") long id, @RequestParam("vcode") String vcode,
			@RequestParam("newPassword") String newPassword, @RequestParam("confirmPassword") String confirmPassword,
			Locale locale) {

		return ResponseEntity.ok(new ApiResponse(true, String.valueOf(userService.updateUserPassword(id, vcode, newPassword, confirmPassword))));
	}

	@PostMapping("/resetPass")
	public ResponseEntity<?> confirmResetPassword(@RequestParam("id") long id,
			@RequestParam("password") String password, @RequestParam("confirmPassword") String confirmPassword,
			@RequestParam("vcode") String vcode, Locale locale) {

		Vcode code = vcodeRepo.findById(id).get();
		if (code != null & code.getCodeBody().equals(vcode)) {

			if (password.equals(confirmPassword)) {

				long reqId = code.getUserRequest().getId();
				UserRequest request = requestRepo.findById(reqId).get();

				if (request != null) {
					if (request.getReqType() == 4) {
						request.setReqStatus(4);
						requestRepo.save(request);
						// reset password in AD
						String samAcc = request.getReqDetails().getSamAccount();
						LdapPerson person = ldapPersonService.find(samAcc);
						if (person != null) {
							System.err.println(person.getDn().toString());
							ldapPersonService.ResetPassword(person, password);
							return ResponseEntity.ok(new ApiResponse(true, String.valueOf(reqId)));
						} else {
							System.err.println("no user found _ NULL");
							return new ResponseEntity<ApiResponse>(
									new ApiResponse(false,
											messageSource.getMessage("users.user.notfound", null, locale)),
									HttpStatus.BAD_REQUEST);
						}
					} else {
						return ResponseEntity.badRequest().body((new ApiResponse(false,
								messageSource.getMessage("requests.bad.request.type", null, locale))));
					}

				} else {
					return new ResponseEntity<ApiResponse>(
							new ApiResponse(false, messageSource.getMessage("requests.not.found", null, locale)),
							HttpStatus.BAD_REQUEST);
				}
			} else {
				return ResponseEntity.badRequest().body(
						(new ApiResponse(false, messageSource.getMessage("users.password.match.error", null, locale))));
			}
		} else {
			System.out.println("Code is invalid !!");
			return ResponseEntity.badRequest()
					.body((new ApiResponse(false, messageSource.getMessage("requests.invalid.code", null, locale))));
		}
	}

}
