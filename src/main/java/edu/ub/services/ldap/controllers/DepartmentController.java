package edu.ub.services.ldap.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.ub.services.ldap.dto.Department;
import edu.ub.services.ldap.dto.OrganizationalUnit;
import edu.ub.services.ldap.repositories.DepartmentRepository;
import edu.ub.services.ldap.response.KeyValueResponse;

@RestController
@RequestMapping("/api/departments")
public class DepartmentController {

	@Autowired
	DepartmentRepository depRepo;
	
	@GetMapping("/ou/{id}/{locale}/")
	public ResponseEntity<?> getOuDepartments(@PathVariable("id") long id, @PathVariable("locale") String locale)
	{
		System.out.println("departments by OU id : " + id);
		System.out.println("departments by OU locale : " + locale);
		List<KeyValueResponse> jsonDepartments = new ArrayList<KeyValueResponse>();
		for(Department dep : depRepo.getDepartmentsByOuId(id))
		{
			if(locale.equals("ar_SA"))
			{
				System.out.println("SA");
				jsonDepartments.add(new KeyValueResponse(String.valueOf(dep.getId()), dep.getDepName_ar()));
			}else
			{
				jsonDepartments.add(new KeyValueResponse(String.valueOf(dep.getId()), dep.getDepName()));
			}
		}			
		return ResponseEntity.ok(jsonDepartments);
	}
	
	@GetMapping("/obj/{id}")
	public Department getOuDepartmentsByLocale(@PathVariable long id)
	{			
		return depRepo.findById(id).get();
	}
}
