package edu.ub.services;

import javax.annotation.PostConstruct;
import javax.naming.InvalidNameException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.ldap.core.LdapTemplate;

import edu.ub.services.ldap.dto.LdapPerson;
import edu.ub.services.ldap.repositories.DepartmentRepository;
import edu.ub.services.ldap.repositories.LdapOURepository;
import edu.ub.services.ldap.repositories.LdapUserRepository;
import edu.ub.services.ldap.service.LdapPersonService;
import edu.ub.services.ldap.service.SmsService;
import edu.ub.services.ldap.service.UserService;


@SpringBootApplication
@EntityScan(basePackageClasses = {UserServiceApplication.class})

public class UserServiceApplication {

	@Autowired
	LdapPersonService ldapPersonService;
	
	@Autowired
	LdapOURepository ouRepo;
	
	@Autowired
	SmsService smsService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	LdapUserRepository ldapUserRepo;
	
	@Autowired
	LdapTemplate ldapTemplate;
	
	
	@Autowired
	DepartmentRepository depRepo;
	@PostConstruct
	void init() throws InvalidNameException {
		//TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
		//Person person = personService.addPerson(new Person("Husseinto", "Emam"));
		//Person person = personService.updatePerson(new Person("Husseinto", "Emamoooo"));
		/*String username = "dev-user1";
		LdapPerson person = ldapPersonService.find("dev-user1");
		System.err.println("User : [ " + username +  "  ] has valid data ??  " + ldapPersonService.updatedAttributes("dev-user1"));
		System.err.println(person.getDn().toString());
		System.err.println(person.getDepartment());
		System.err.println(depRepo.getDepartmentIdByLiteralValue("Rec_Off"));		
		ldapPersonService.findOu("Cen_Med");*/
	}

	public static void main(String[] args) {
		//System.setProperty("javax.net.debug", "ssl"); 
		//System.setProperty("javax.net.ssl.trustStore", "C:\\Program Files\\Java\\jdk1.8.0_102\\jre\\lib\\security\\cacerts");
		SpringApplication.run(UserServiceApplication.class, args);
	}
}
