/* Organizational Units */
INSERT INTO `user_service`.`ou` (`ou_name`, `ou_name_ar`, `ou_name_ad`) VALUES ('Rectorship', 'إداراة الجامعة','Rectorship');
INSERT INTO `user_service`.`ou` (`ou_name`, `ou_name_ar`, `ou_name_ad`) VALUES ('Centers', 'المراكز والمعاهد','Centers');
INSERT INTO `user_service`.`ou` (`ou_name`, `ou_name_ar`, `ou_name_ad`) VALUES ('Administrations', 'الادارات','Administrations');
INSERT INTO `user_service`.`ou` (`ou_name`, `ou_name_ar`, `ou_name_ad`) VALUES ('Colleges', 'الكليات','Colleges');
INSERT INTO `user_service`.`ou` (`ou_name`, `ou_name_ar`, `ou_name_ad`) VALUES ('Deanships', 'العمادات','Deanships');
INSERT INTO `user_service`.`ou` (`ou_name`, `ou_name_ar`, `ou_name_ad`) VALUES ('Vice Rectors', 'وكالات الجامعة','ViceRectors');


/*RectorShip - 1 */
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Rector Office', 'مكتب مدير الجامعة', '1','Rec_Off');

/*Centers - 2 */
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Media Center', 'المركز الاعلامي', '2','Cen_Med');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Leadership Development Center', 'مركز اعداد القيادات', '2','Cen_Lead_Dev');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Innovation and Entrepreneurship Center', 'مركز الابداع وريادة الاعمال', '2','Cen_Inn_Entr');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Center for Documentation and Archives', 'مركز الوثائق والمحفوظات', '2','Cen_Doc_Arch');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Research and Consulting Institute', 'معهد البحوث والاستشارات', '2','Inst_Res_Cons');

/*Administrations - 3 */
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Administration of Financial Affairs', 'الادارة المالية', '3','ADM_Fin');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Administration of Contracts and Procurement', 'العقود والمشتريات', '3','ADM_Con');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Administration of Information Technology', 'تقنية المعلومات', '3','ADM_IT');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Administration of Projects', 'المشاريع', '3','ADM_Proj');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Administration of Planning and Budget', 'التخطيط والميزانية', '3','ADM_Plann');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Administration of Security', 'الامن', '3','ADM_Secur');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Administration of Public Relations', 'العلاقات العامة', '3','ADM_PublicR');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Administration of Services and Maintenance', 'التشغيل والصيانة', '3','ADM_Serv_M');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Administration of Legal Affairs', 'القانونية', '3','ADM_Legal');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Administration of Stores', 'المستودعات', '3','ADM_Store');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Administration of Inventory Control', 'مراقبة المخزون', '3','ADM_Inventory');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Administration of Scholarship and Training', 'الابتعاث والتدريب', '3','ADM_Scholar');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Administration of Development of Higher Education', 'تطوير التعليم', '3','ADM_Dev_HiEdu');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Administration of Student Fund', 'صندوق الطالب', '3','ADM_Stu_Fund');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Administration of Interior revision', 'المراجعة الداخلية', '3','ADM_Interior');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Administration of Follow up Department', 'المتابعة', '3','ADM_Follow');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Administration of Educational Equipment', 'التجهيزات التعليمية', '3','ADM_Edu_Equip');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Administration of Investment', 'الاستثمار', '3','ADM_Invest');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Administration of Studies Information', 'الدراسات والمعلومات', '3','ADM_Studies');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Administration of Traffic Management', 'الحركة والنقل', '3','ADM_Traffic');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Administration of Development Management', 'التطوير الاداري', '3','ADM_Dev_Mgmt');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Administration of Measurement Evaluation', 'القياس والتقويم', '3','ADM_Measure');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Administration of International Cooperation', 'التعاون الدولي', '3','ADM_International');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Administration of Intellectual Awareness', 'التوعية الفكرية', '3','ADM_Int_Awareness');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Administration of Saftey', 'السلامة', '3','ADM_Safe');


/*Colleges - 4 */
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Faculty of art and sciences in tathleeth', 'كلية العلوم والاداب بتثليث', '4','COL_ArtART_tath');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Faculty of Arts', 'كلية الاداب', '4','COL_Arts');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Faculty Of Business', 'كلية الاعمال', '4','COL_Busin');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Faculty of Community', 'كلية المجتمع', '4','COL_Commu');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Faculty of Community in Alnamas', 'كلية المجتمع بالنماص', '4','COL_Comm_nms');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Faculty Of Computer Science and Information Technology', 'كلية الحاسبات وتقنية المعلومات', '4','COL_Comp_IT');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Faculty of Education', 'كلية التربية', '4','COL_Educa');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Faculty of Engineering', 'كلية الهندسة', '4','COL_Engin');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Faculty of Home_Economics', 'كلية الاقتصاد المنزلي', '4','COL_HomeECO');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Faculty of Medical Applied Sciences', 'كلية العلوم الطبية التطبيقية', '4','COL_Med_AS');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Faculty of Medicine', 'كلية الطب', '4','COL_Medic');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Faculty Of Sciences', 'كلية العلوم', '4','COL_Scien');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Faculty of Sciences and Arts in Belgarn', 'كلية العلوم والاداب ببلقرن', '4','COL_SciART_sbt');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Faculty of Sciences and Arts in Alnamas', 'كلية العلوم والاداب بالنماص', '4','COL_SciART_nms');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Faculty of Medical Applied Sciences in Alnamas', 'كلية العلوم الطبية التطبيقية بالنماص', '4','COL_Med_AS_nms');


/*Deanships - 5 */
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Deanship of Admission and Registration', 'عمادة القبول والتسجيل', '5','DEN_Adm');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Deanship of Graduate Studies', 'عمادة الدراسات العليا', '5','DEN_Grad');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Deanship of Scientific Research', 'عمادة البحث العلمي', '5','DEN_Sci');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Deanship Of E-Learning', 'عمادة التعلم الالكتروني', '5','DEN_eLe');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Deanship of Development and Quality', 'عمادة التطوير والجودة', '5','DEN_Dev');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Deanship of Community Service', 'عمادة خدمة المجتمع', '5','DEN_Comm');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Deanship of Student Affairs', 'عمادة شؤون الطلاب', '5','DEN_Stu');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Deanship of Library Affairs', 'عمادة شئون المكتبات', '5','DEN_Lib');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Deanship of HR and Personnel Affairs', 'عمادة شؤون اعضاء هيئة التدريس والموظفين', '5','DEN_HR');

/*ViceRectors - 6 */
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Academic Affairs', 'وكالة الجامعة للشؤون التعليمية', '6','REC_vAcademic');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('V-Graduate Studies and Scientific Research', 'وكالة الجامعة للدراسات العليا والبحث العلمي', '6','REC_vGraduate');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Vice Rector', 'وكالة الجامعة', '6','REC_ViceRect');
INSERT INTO `user_service`.`department` (`dep_name`, `dep_name_ar`, `ou`, `dep_name_ad`) VALUES ('Development and Quality', 'وكالة الجامعة للتطوير والجودة', '6','REC_vDeQLTY');

